#include "MenuButton.h"

MenuButton::MenuButton(const LoaderParams* pParams, void(*callback)()) :
	SDLGameObject(pParams)
{
	m_currentFrame = MOUSE_OUT;
	m_callback = callback;
	m_bReleased = false;
}

void MenuButton::Draw()
{
	SDLGameObject::Draw();
}

void MenuButton::Update(float dt)
{
	vec2* pMousePos = TheInputHandler::Instance()->getMousePosition();

	if (pMousePos->x < (m_postition.x + m_width)
		&& pMousePos->x > m_postition.x
		&& pMousePos->y < (m_postition.y + m_height)
		&& pMousePos->y > m_postition.y)
	{
		if (TheInputHandler::Instance()->getMouseState(LEFT)&&!m_bReleased)
		{
			m_bReleased = true;
			m_callback();
			m_currentFrame = CLICK;
		}
		else if (!TheInputHandler::Instance()->getMouseState(LEFT))
		{
			m_bReleased = false;
			m_currentFrame = MOUSE_OVER;
		}
	}
	else
	{
		m_currentFrame = MOUSE_OUT;
	}
}

void MenuButton::Clean()
{
	TheInputHandler::Instance()->reset();
	SDLGameObject::Clean();
}

void MenuButton::Load(const LoaderParams* pParams)
{
	SDLGameObject::Load(pParams);
	m_callbackId = pParams->getCallBackId();
	m_currentFrame = MOUSE_OUT;
	m_bReleased = false;
}

GameObject* MenuButtonFactory::CreateGameObject() const
{
	return new MenuButton();
}
