#pragma once
#include "SDLGameObject.h"
#include "InputHandler.h"
#include "GameObjectFactory.h"
class MenuButtonFactory :public BaseCreator
{
public:
	virtual GameObject* CreateGameObject() const override;
};
class MenuButton :public SDLGameObject
{
public:
	MenuButton() :m_bReleased(false) {}
	MenuButton(const LoaderParams* pParams,void(*callback)());

	virtual void Draw();
	virtual void Update(float dt);
	virtual void Clean();

	virtual void Load(const LoaderParams* pParams) override;

	void SetCallBack(void(*callback)()) { m_callback = callback; }
	int GetCallBackID() const { return m_callbackId; }
private:
	bool m_bReleased;
	void (*m_callback)();
	enum button_state
	{
		MOUSE_OUT = 0,
		MOUSE_OVER = 1,
		CLICK = 2,
	};
};

