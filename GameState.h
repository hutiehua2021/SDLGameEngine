#pragma once
#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_
#include <string>
#include <iostream>
#include <vector>
class GameState
{
public:
	virtual void update(float dt) = 0;
	virtual void renderer() = 0;

	virtual bool onEnter() = 0;
	virtual bool onExit() = 0;

	virtual const std::string& getStateID() const = 0;

protected:
	std::vector<std::string> m_textureIDList;
};
#endif	//_GAMESTATE_H_