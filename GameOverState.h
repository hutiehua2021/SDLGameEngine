#pragma once
#ifndef _GAMEOVERSTATE_H_
#define _GAMEOVERSTATE_H_
#include "MenuState.h"
#include "StateParser.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "Game.h"
class GameOverState:public MenuState
{
public:
	~GameOverState();
	virtual void update(float dt);
	virtual void renderer();

	virtual bool onEnter();
	virtual bool onExit();

	virtual const std::string& getStateID() const;
private:
	virtual void SetCallBack(const std::vector<Callback>& callbacks) override;

	static void s_gameoverToMain();
	static void s_restartPlay();
	static const std::string s_gameoverID;
	std::vector<GameObject*> m_gameObjects;
};

#endif	//_GAMEOVERSTATE_H_

