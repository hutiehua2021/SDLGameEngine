#include "MenuState.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
const std::string MenuState::s_menuID = "MENU";

MenuState::~MenuState()
{
	m_gameObjects.clear();
}

void MenuState::update(float dt)
{
	for (auto iter:m_gameObjects)
		iter->Update(dt);
}

void MenuState::renderer()
{
	for (auto iter : m_gameObjects)
		iter->Draw();
}

bool MenuState::onEnter()
{
	//��������
	GameObject* button1 = new MenuButton(new LoaderParams(100, 100,
		400, 100, "playbutton"), s_menuToPlay);
	GameObject* button2 = new MenuButton(new LoaderParams(100, 300,
		400, 100, "exitbutton"), s_exitFromMenu);

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);
	std::cout << "entering MenuState\n";
	return true;
}

bool MenuState::onExit()
{
	for (auto iter:m_gameObjects)
	{
		iter->Clean();	
	}
	m_gameObjects.clear();
	TextureManager::Instance()->ClearFromTextureMap("playbutton");
	TextureManager::Instance()->ClearFromTextureMap("exitbutton");
	std::cout << "exiting MenuState\n";
	return true;
}

const std::string& MenuState::getStateID() const
{
	return s_menuID;
}

void MenuState::s_menuToPlay()
{
	Game::Instance()->getGameStateMachine()->changeState(new PlayState());
}

void MenuState::s_exitFromMenu()
{
	Game::Instance()->Quit();
}
