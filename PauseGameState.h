#pragma once
#ifndef _PAUSEGAMESTATE_H_
#define _PAUSEGAMESTATE_H_
#include <vector>
#include "MenuState.h"
#include "StateParser.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "Game.h"
class PauseGameState:public MenuState
{
public:
	~PauseGameState();
	virtual void update(float dt);
	virtual void renderer();

	virtual bool onEnter();
	virtual bool onExit();

	virtual const std::string& getStateID() const;
private:
	virtual void SetCallBack(const std::vector<Callback>& callbacks) override;

	static void s_pauseToMain();
	static void s_resumePlay();
	static const std::string s_pauseID;
	std::vector<GameObject*> m_gameObjects;
};

#endif	//_PAUSEGAMESTATE_H_

