#include "Game.h"
#include "Light.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "SoundManager.h"
#include "Cube.h"
Game* Game::s_pInstance = 0;
Game::~Game()
{
	InputHandler::Instance()->Clean();
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
	delete m_pGameStateMachine;
}
GameStateMachine* Game::getGameStateMachine() const
{
	return m_pGameStateMachine;
}
bool Game::Init(const char* title, int x, int y, int width, int height,bool fullScreen)
{
	int flag = 0;
	if (fullScreen)
		flag = SDL_WINDOW_FULLSCREEN;
	//初始化SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) >= 0)
	{
		//如果成功那么创建我们的窗口
		std::cout << "SDL 初始化成功\n";
		m_pWindow= SDL_CreateWindow(title, x, y, width, height, flag);
		//如果窗口创建成功，那么创建我们的渲染器
		if (m_pWindow != 0)
		{
			std::cout << "window 创建成功\n";
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);
			if (m_pRenderer!=0)
			{
				std::cout << "renderer 创建成功\n";
				SDL_SetRenderDrawColor(m_pRenderer, 0x00, 0x00, 0x00, 0xff);
			}
			else
			{
				std::cout << "render 创建失败\n";
				return false;
			}
			m_pSurface = SDL_GetWindowSurface(m_pWindow);
			if (m_pSurface!=0)
			{
				std::cout << "Surface 创建成功\n";
			}
			else
			{
				std::cout << "Surface 创建失败\n";
				return false;
			}
			m_pTexture = SDL_CreateTextureFromSurface(m_pRenderer,m_pSurface);
			if (m_pTexture!=0)
			{
				std::cout << "Texture 创建成功\n";
			}
			else
			{
				std::cout << "Texture 创建失败\n";
				return false;
			}
		}
		else
		{
			std::cout << "SDL Window 初始化失败\n";
			return false;
		}
	}
	else
	{
		std::cout << "SDL Init 初始化失败\n";
		return false;
	}
	std::cout << "SDL 初始化成功\n";
	/////////////////////////////////初始化功能函数
	InputHandler::Instance()->InitialiseJoysticks();
	Build_Sin_Cos_Tables();
	Reset_Lights_LIGHTV1();
	//初始化图元
	primitive = new Primitive2D(m_pRenderer, m_pSurface, m_pTexture);
	vec2 min(0, 0);
	vec2 max(width - 1, height - 1);
	primitive->SetClipRect(min, max);
	//初始化相机
	camera = new FirstPersonCamera(width, height);
	////////////////////////////////////////////////

	m_bRunning = true;
	m_ScreenHeight = height;
	m_ScreenWidth = width;

	GameObjectFactory::Instance()->RegisterType("MenuButton", new MenuButtonFactory());
	GameObjectFactory::Instance()->RegisterType("Player", new PlayerFactory());
	GameObjectFactory::Instance()->RegisterType("Cube", new CubeFactory());
	SoundManager::Instance()->Load("assets/phaser.wav","shot",sound_type::SOUND_SFX);

	m_pGameStateMachine = new GameStateMachine();
	m_pGameStateMachine->changeState(new MainMenuState());
	return true;
}

void Game::Render()
{
	SDL_SetRenderDrawColor(m_pRenderer, 0x87, 0xce, 0xeb, 0x00);
	SDL_RenderClear(m_pRenderer);
	RenderList::Instance()->ResetRenderList();
	m_pGameStateMachine->renderer();
	RenderList::Instance()->DrawRenderList(camera, primitive);
	SDL_RenderPresent(m_pRenderer);
}

void Game::Update(float dt)
{
	m_pGameStateMachine->update(dt);
}

void Game::HandleEvent(float dt)
{
	TheInputHandler::Instance()->Update();
	dt /= 1000;
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_W))
	{
		camera->ProcessKeyBroad(CAM_FORWARD, dt);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_S))
	{
		camera->ProcessKeyBroad(CAM_BACKWARD, dt);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_A))
	{
		camera->ProcessKeyBroad(CAM_LEFT, dt);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_D))
	{
		camera->ProcessKeyBroad(CAM_RIGHT, dt);
	}
	camera->MoveCameraMouse(TheInputHandler::Instance()->getMousePosition()->x, TheInputHandler::Instance()->getMousePosition()->y);
}

void Game::Clean()
{
	InputHandler::Instance()->Clean();
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}

void Game::Quit()
{
	m_bRunning = false;
	std::cout << "结束游戏引擎\n";
}

