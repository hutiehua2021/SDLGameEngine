#pragma once
#include <vector>
#include "GameState.h"
#include "GameObject.h"
class MenuState :public GameState
{
public:
	~MenuState();
	virtual void update(float dt);
	virtual void renderer();

	virtual bool onEnter();
	virtual bool onExit();

	virtual const std::string& getStateID() const;
private:
	static void s_menuToPlay();
	static void s_exitFromMenu();
	static const std::string s_menuID;
	std::vector<GameObject*> m_gameObjects;

protected:
	typedef void(*Callback)();
	virtual void SetCallBack(const std::vector<Callback>& callbacks) = 0;
	std::vector<Callback> m_callbacks;
};

