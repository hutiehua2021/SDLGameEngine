#include "Player.h"
#include "Game.h"

void Player::Load(const LoaderParams* pParams)
{
	SDLGameObject::Load(pParams);
}
void Player::Draw()
{
}

void Player::Update(float dt)
{	

}

void Player::Clean()
{
	delete primitive;
}

void Player::InputHandle()
{
	
}
GameObject* PlayerFactory::CreateGameObject() const
{
	return new Player();
}
