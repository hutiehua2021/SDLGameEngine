#include "GameOverState.h"
#include "MainMenuState.h"
const std::string GameOverState::s_gameoverID = "GAMEOVER";
GameOverState::~GameOverState()
{
	onExit();
}

void GameOverState::update(float dt)
{
	for (auto iter : m_gameObjects)
		iter->Update(dt);
}

void GameOverState::renderer()
{
	for (auto iter : m_gameObjects)
		iter->Draw();
}

bool GameOverState::onEnter()
{
	StateParser stateParser;
	stateParser.ParseState("test.xml", s_gameoverID, m_gameObjects, m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_gameoverToMain);
	m_callbacks.push_back(s_restartPlay);

	SetCallBack(m_callbacks);

	std::cout << "Enter Game Over State\n";
	return true;
}

bool GameOverState::onExit()
{
	for (auto iter : m_gameObjects)
		iter->Clean();
	m_gameObjects.clear();
	for (size_t i = 0; i < m_textureIDList.size(); i++)
	{
		TextureManager::Instance()->ClearFromTextureMap(m_textureIDList[i]);
	}
	m_textureIDList.clear();
	return true;
}

const std::string& GameOverState::getStateID() const
{
	return s_gameoverID;
}

void GameOverState::SetCallBack(const std::vector<Callback>& callbacks)
{
	for (size_t i = 0; i < m_gameObjects.size(); i++)
	{
		if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
		{
			MenuButton* button = dynamic_cast<MenuButton*>(m_gameObjects[i]);
			button->SetCallBack(callbacks[button->GetCallBackID()]);
		}
	}
}

void GameOverState::s_gameoverToMain()
{
	Game::Instance()->getGameStateMachine()->changeState(new MainMenuState());
}

void GameOverState::s_restartPlay()
{
	Game::Instance()->getGameStateMachine()->changeState(new PlayState());
}
