#include "vec3.h"

inline vec3& vec3::operator+=(const vec3& v2)
{
	// TODO: 在此处插入 return 语句
	e[0] += v2.e[0];
	e[1] += v2.e[1];
	e[2] += v2.e[2];
	return *this;
}

inline vec3& vec3::operator-=(const vec3& v2)
{
	// TODO: 在此处插入 return 语句
	e[0] -= v2.e[0];
	e[1] -= v2.e[1];
	e[2] -= v2.e[2];
	return *this;
}

inline vec3& vec3::operator*=(const vec3& v2)
{
	// TODO: 在此处插入 return 语句
	e[0] *= v2.e[0];
	e[1] *= v2.e[1];
	e[2] *= v2.e[2];
	return *this;
}

inline vec3& vec3::operator/=(const vec3& v2)
{
	// TODO: 在此处插入 return 语句
	e[0] /= v2.e[0];
	e[1] /= v2.e[1];
	e[2] /= v2.e[2];
	return *this;
}

inline vec3& vec3::operator*=(const float t)
{
	// TODO: 在此处插入 return 语句
	e[0] *= t;
	e[1] *= t;
	e[2] *= t;
	return *this;
}

inline vec3& vec3::operator/=(const float t)
{
	// TODO: 在此处插入 return 语句
	float k = 1.0 / t;

	e[0] *= k;
	e[1] *= k;
	e[2] *= k;
	return *this;
}
inline void vec3::make_uint_vector()
{
	float k = 1.0 / sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]);
	e[0] *= k;
	e[1] *= k;
	e[2] *= k;
}
