#include "InputHandler.h"
#include <iostream>
//#include "Game.h"
InputHandler* InputHandler::s_pInstance = 0;

void InputHandler::InitialiseJoysticks()
{
	//��ʼ��xbox�ֱ�
	{
		if (SDL_WasInit(SDL_INIT_JOYSTICK) == 0)
		{
			SDL_InitSubSystem(SDL_INIT_JOYSTICK);
		}

		if (SDL_NumJoysticks() > 0)
		{
			for (size_t i = 0; i < SDL_NumJoysticks(); i++)
			{
				SDL_Joystick* joy = SDL_JoystickOpen(i);
				m_joysticks.push_back(joy);
				m_joysticksValus.push_back(std::make_pair(new vec2(0, 0), new vec2(0, 0)));

				std::vector<bool> tempButtonState;
				for (size_t i = 0; i < SDL_JoystickNumButtons(joy); i++)
				{
					tempButtonState.push_back(false);
				}
				m_buttonStates.push_back(tempButtonState);
			}
			SDL_JoystickEventState(SDL_ENABLE);
			m_bJoysticksInitialised = true;
			std::cout << "initialised " << m_joysticks.size() << "joystick(s)\n";
		}
		else
		{
			m_bJoysticksInitialised = false;
		}
	}
	//��ʼ�����
	{
		for (size_t i=0;i<3;++i)
		{
			m_mouseState.push_back(false);
		}
		m_mousePosition = new vec2();
	}
}
//joy ��ʾ�ֱ���ţ�stick ��ʾ����Ҳ��ݸ�
int InputHandler::xvalue(int joy, int stick) const
{
	if (m_joysticksValus.size()>0)
	{
		if (stick==1)
		{
			return m_joysticksValus[joy].first->x;
		}
		else if (stick==2)
		{
			return m_joysticksValus[joy].second->x;
		}
	}
	return 0;
}
//joy ��ʾ�ֱ���ţ�stick ��ʾ����Ҳ��ݸ�
int InputHandler::yvalue(int joy, int stick) const
{
	if (m_joysticksValus.size() > 0)
	{
		if (stick == 1)
		{
			return m_joysticksValus[joy].first->y;
		}
		else if (stick == 2)
		{
			return m_joysticksValus[joy].second->y;
		}
	}
	return 0;
}
//��ȡ�ֱ�����״̬
bool InputHandler::getButtonState(int joy, int buttonNumber) const
{
	return m_buttonStates[joy][buttonNumber];
}
//��ȡ���״̬
bool InputHandler::getMouseState(int buttonNumber) const
{
	return m_mouseState[buttonNumber];
}
//��ȡ���λ��
vec2* InputHandler::getMousePosition() const
{
	return m_mousePosition;
}
//��ȡ���̰���״̬
bool InputHandler::isKeyDown(SDL_Scancode key) const
{
	if (m_keyState!=0)
	{
		if (m_keyState[key] == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
//�������״̬
void InputHandler::reset()
{
	m_mouseState[LEFT] = false;
	m_mouseState[RIGHT] = false;
	m_mouseState[MIDDLE] = false;
}

void InputHandler::Update()
{
	m_keyState = SDL_GetKeyboardState(0);
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			break;
		case SDL_JOYAXISMOTION:
			onJoystickAxisMove(event);
			break;
		case SDL_JOYBUTTONDOWN:
			onJoystickButtonDown(event);
			break;
		case SDL_JOYBUTTONUP:
			onJoystickButtonUp(event);
			break;
		case SDL_MOUSEMOTION:
			onMouseMove(event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			onMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseButtonUp(event);
			break;
		default:
			break;
		}
	}
}

void InputHandler::Clean()
{
	if (m_bJoysticksInitialised)
	{
		for (size_t i=0;i< SDL_NumJoysticks();i++)
		{
			SDL_JoystickClose(m_joysticks[i]);
		}
	}
}

InputHandler::~InputHandler()
{
	delete m_keyState;
	delete m_keyState;

	m_joysticks.clear();
	m_joysticksValus.clear();
	m_buttonStates.clear();
	m_mouseState.clear();
}

void InputHandler::onMouseMove(SDL_Event& event)
{
	m_mousePosition->x =event.motion.x;
	m_mousePosition->y =event.motion.y;
}

void InputHandler::onMouseButtonDown(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT)
	{
		m_mouseState[LEFT] = true;
	}
	if (event.button.button == SDL_BUTTON_MIDDLE)
	{
		m_mouseState[MIDDLE] = true;
	}
	if (event.button.button == SDL_BUTTON_RIGHT)
	{
		m_mouseState[RIGHT] = true;
	}
}

void InputHandler::onMouseButtonUp(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT)
	{
		m_mouseState[LEFT] = false;
	}
	if (event.button.button == SDL_BUTTON_MIDDLE)
	{
		m_mouseState[MIDDLE] = false;
	}
	if (event.button.button == SDL_BUTTON_RIGHT)
	{
		m_mouseState[RIGHT] = false;
	}
}

void InputHandler::onJoystickAxisMove(SDL_Event& event)
{
	int whichOne = event.jaxis.which;
	//��ߵĲ��ݸ�, ����������ƶ�
	if (event.jaxis.axis == 0)
	{
		if (event.jaxis.value > m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].first->x = 1;
		}
		else if (event.jaxis.value < -m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].first->x =-1;
		}
		else
		{
			m_joysticksValus[whichOne].first->x = 0;
		}
	}
	//��ߵĲ��ݸˣ��Ϻ���
	if (event.jaxis.axis == 1)
	{
		if (event.jaxis.value > m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].first->y = 1;
		}
		else if (event.jaxis.value < -m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].first->y =-1;
		}
		else
		{
			m_joysticksValus[whichOne].first->y = 0;
		}
	}
	//�ұߵĲ��ݸˣ������
	if (event.jaxis.axis == 3)
	{
		if (event.jaxis.value > m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].second->x = 1;
		}
		else if (event.jaxis.value < -m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].second->x = -1;
		}
		else
		{
			m_joysticksValus[whichOne].second->x = 0;
		}
	}
	//�ұߵĲ��ݸˣ��Ϻ���
	if (event.jaxis.axis == 4)
	{
		if (event.jaxis.value > m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].second->y = 1;
		}
		else if (event.jaxis.value < -m_joystickDeadZone)
		{
			m_joysticksValus[whichOne].second->y = -1;
		}
		else
		{
			m_joysticksValus[whichOne].second->y = 0;
		}
	}
}

void InputHandler::onJoystickButtonDown(SDL_Event& event)
{
	int whichOne = event.jaxis.which;

	m_buttonStates[whichOne][event.jbutton.button] = true;
}

void InputHandler::onJoystickButtonUp(SDL_Event& event)
{
	int whichOne = event.jaxis.which;

	m_buttonStates[whichOne][event.jbutton.button] = false;
}
