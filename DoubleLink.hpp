#ifndef DOUBLE_LINK_HXX
#define DOUBLE_LINK_HXX

#include <iostream>
#include <functional>
using namespace std;
//一个节点
template<class T>
struct DNode
{
public:
	T value;
	DNode* prev = 0;
	DNode* next = 0;
public:
	DNode():prev(0),next(0),value(0) { }
	DNode(T t, DNode* prev = 0, DNode* next = 0) {
		this->value = t;
		this->prev = prev;
		this->next = next;
	}
};
//双向链表基本操作
template<class T>
class DoubleLink
{
public:
	DoubleLink();
	~DoubleLink();

	int size();//大小
	int is_empty();//判断是否为空

	DNode<T>* get(DNode<T>* value);
	T get(int index);//获取节点值
	T get_first();//获取首节点值
	T get_last();//获取尾节点值
	DNode<T>* getNode(int index);	//获取节点node
	DNode<T>* get_firstNode();	//获取首节点node
	DNode<T>* get_lastNode(); //获取尾节点node

	int insert(int index, T t);
	int insert_first(T t);
	int append_last(T t);

	int del(int index);
	int del(DNode<T>* value);
	int delete_first();
	int delete_last();
	template<class F>
	void for_each(F const& cmp);

private:
	int count;
	DNode<T>* phead = 0;
private:
	DNode<T>* get_node(int index);
};

template<class T>
DoubleLink<T>::DoubleLink() : count(0)
{
	// 创建“表头”。注意：表头没有存储数据！
	phead = new DNode<T>();
	phead->prev = phead->next = phead;
	// 设置链表计数为0
	//count = 0;
}

// 析构函数
template<class T>
DoubleLink<T>::~DoubleLink()
{
	// 删除所有的节点
	DNode<T>* ptmp;
	DNode<T>* pnode = phead->next;
	while (pnode != phead)
	{
		ptmp = pnode;
		pnode = pnode->next;
		delete ptmp;
	}

	// 删除"表头"
	delete phead;
	phead = NULL;
}

// 返回节点数目
template<class T>
int DoubleLink<T>::size()
{
	return count;
}

// 返回链表是否为空
template<class T>
int DoubleLink<T>::is_empty()
{
	return count == 0;
}

template <class T>
DNode<T>* DoubleLink<T>::get(DNode<T>* value)
{
	DNode<T>* pnode = get_node(0);
	while(pnode!=0||pnode == value)
	{
		pnode = pnode->next;
	}
	return pnode;
}

// 获取第index位置的节点
template<class T>
DNode<T>* DoubleLink<T>::get_node(int index)
{
	// 判断参数有效性
	if (index < 0 || index >= count)
	{
		cout << "get node failed! the index in out of bound!" << endl;
		return NULL;
	}

	// 正向查找
	if (index <= count / 2)
	{
		int i = 0;
		DNode<T>* pindex = phead->next;
		while (i++ < index) {
			pindex = pindex->next;
		}

		return pindex;
	}

	// 反向查找
	int j = 0;
	int rindex = count - index - 1;
	DNode<T>* prindex = phead->prev;
	while (j++ < rindex) {
		prindex = prindex->prev;
	}

	return prindex;
}

// 获取第index位置的节点的值
template<class T>
T DoubleLink<T>::get(int index)
{
	return get_node(index)->value;
}
// 获取第index位置的节点
template <class T>
DNode<T>* DoubleLink<T>::getNode(int index)
{
	return get_node(index);
}
// 获取第1个节点
template <class T>
DNode<T>* DoubleLink<T>::get_firstNode()
{
	return get_node(0);
}

// 获取第1个节点的值
template<class T>
T DoubleLink<T>::get_first()
{
	return get_node(0)->value;
}
// 获取最后一个节点
template <class T>
DNode<T>* DoubleLink<T>::get_lastNode()
{
	return get_node(count - 1);
}

// 获取最后一个节点的值
template<class T>
T DoubleLink<T>::get_last()
{
	return get_node(count - 1)->value;
}



// 将节点插入到第index位置之前
template<class T>
int DoubleLink<T>::insert(int index, T t)
{
	if (index == 0)
		return insert_first(t);

	DNode<T>* pindex = get_node(index);
	DNode<T>* pnode = new DNode<T>(t, pindex->prev, pindex);
	pindex->prev->next = pnode;
	pindex->prev = pnode;
	count++;

	return 0;
}

// 将节点插入第一个节点处。
template<class T>
int DoubleLink<T>::insert_first(T t)
{
	DNode<T>* pnode = new DNode<T>(t, phead, phead->next);
	phead->next->prev = pnode;
	phead->next = pnode;
	count++;

	return 0;
}

// 将节点追加到链表的末尾
template<class T>
int DoubleLink<T>::append_last(T t)
{
	DNode<T>* pnode = new DNode<T>(t, phead->prev, phead);
	phead->prev->next = pnode;
	phead->prev = pnode;
	count++;

	return 0;
}

// 删除index位置的节点
template<class T>
int DoubleLink<T>::del(int index)
{
	DNode<T>* pindex = get_node(index);
	pindex->next->prev = pindex->prev;
	pindex->prev->next = pindex->next;
	delete pindex;
	count--;

	return 0;
}

template <class T>
int DoubleLink<T>::del(DNode<T>* value)
{
	value->next->prev = value->prev;
	value->prev->next = value->next;
	delete value;
	value = 0;
	count--;

	return 0;
}

// 删除第一个节点
template<class T>
int DoubleLink<T>::delete_first()
{
	return del(0);
}

// 删除最后一个节点
template<class T>
int DoubleLink<T>::delete_last()
{
	return del(count - 1);
}

template <class T>
template <class F>
void DoubleLink<T>::for_each(F const& cmp)
{
	DNode<T>* pnode = get_node(0);
	int index = 0;
	while(index<count)
	{
		cmp(pnode);
		pnode = pnode->next;
		++index;
	}

}

#endif
