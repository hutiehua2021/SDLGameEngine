#pragma once
#ifndef _RENDERLIST_H_
#define _RENDERLIST_H_
#include "Primitive3D.h"
#include "FirstPersonCamera.h"
#include "Light.h"

class RenderList
{
public:
	//单例
	static RenderList* Instance()
	{
		if (p_sInstance == 0)
			p_sInstance = new RenderList();
		return p_sInstance;
	}
	//绘制渲染管线
	void DrawRenderList(FirstPersonCamera* cam,Primitive2D* primitive);
	//初始化渲染管线
	void ResetRenderList();
	//将物体插入到渲染列表中
	void InsertRenderList(OBJECT4DV1_PTR obj, FirstPersonCamera* camera);
private:
	RENDERLIST4DV1 rend_list;
	FirstPersonCamera* m_camera;
	static RenderList* p_sInstance;
	RenderList() = default;
};
#endif // !_RENDERLIST_H_


