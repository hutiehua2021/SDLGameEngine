#pragma once
#ifndef _INPUT_HANDLER_H_
#define _INPUT_HANDLER_H_
#include<vector>
#include <map>
#include <SDL.h>
#include "EngineMath.h"
enum mouse_button
{
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2
};
class InputHandler
{
public:
	static InputHandler* Instance()
	{
		if (s_pInstance == 0)
			s_pInstance = new InputHandler();
		return s_pInstance;
	}

	void InitialiseJoysticks();
	bool joysticksInitialised() const { return m_bJoysticksInitialised; }

	int xvalue(int joy,int stick) const;
	int yvalue(int joy, int stick) const;

	bool getButtonState(int joy,int buttonNumber) const;
	bool getMouseState(int buttonNumber) const;  
	vec2* getMousePosition() const;
	bool isKeyDown(SDL_Scancode key) const;
	void reset();

	void Update();
	void Clean();
private:
	InputHandler() {}
	~InputHandler();
	static InputHandler* s_pInstance;
	const int m_joystickDeadZone = 10000;
	std::vector<SDL_Joystick*> m_joysticks;
	std::vector<std::pair<vec2*, vec2*>> m_joysticksValus; //两个操纵杆（左和右）
	std::vector<std::vector<bool>> m_buttonStates;//对每个控制器都存储对应的每个按键的状态
	std::vector<bool> m_mouseState;	//鼠标按键状态
	vec2* m_mousePosition;	//鼠标位置
	const Uint8* m_keyState = 0;	//键盘状态
	bool m_bJoysticksInitialised;
private:
	void onKeyDown();
	void onKeyUp();

	void onMouseMove(SDL_Event& event);
	void onMouseButtonDown(SDL_Event& event);
	void onMouseButtonUp(SDL_Event& event);

	void onJoystickAxisMove(SDL_Event& event);
	void onJoystickButtonDown(SDL_Event& event);
	void onJoystickButtonUp(SDL_Event& event);
};
typedef InputHandler TheInputHandler;
#endif	//_INPUT_HANDLER_H_
