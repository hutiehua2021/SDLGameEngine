#include "GameObjectFactory.h"

GameObjectFactory* GameObjectFactory::s_pInstance = 0;
bool GameObjectFactory::RegisterType(const std::string& typeId, BaseCreator* pCreator)
{
	std::map<std::string, BaseCreator*>::iterator iter = m_creators.find(typeId);
	if (iter!=m_creators.end())
	{
		delete pCreator;
		std::cout << __FILE__<<"\n" << __FUNCTION__<<"\n" << "找到已存在类型："<<typeId<<"\n";
		return false;
	}

	m_creators[typeId] = pCreator;

	return true;
}

GameObject* GameObjectFactory::Create(const std::string& typeId)
{
	std::map<std::string, BaseCreator*>::iterator iter = m_creators.find(typeId);
	if (iter==m_creators.end())
	{
		std::cout << __FILE__ <<"\n"<< __FUNCTION__ <<"\n"<< "不存在类型"<<typeId<<"\n";
		return nullptr;
	}

	BaseCreator* pCreator = (*iter).second;
	return pCreator->CreateGameObject();
}

GameObjectFactory::~GameObjectFactory()
{
	m_creators.clear();
}
