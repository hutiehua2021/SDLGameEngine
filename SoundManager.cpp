#include "SoundManager.h"

SoundManager* SoundManager::p_sInstance = 0;

SoundManager::SoundManager()
{
	Mix_OpenAudio(22050,AUDIO_S16,2,4096);
}

SoundManager::~SoundManager()
{
	Mix_CloseAudio();
	m_music.clear();
	m_sfxs.clear();
}

bool SoundManager::Load(const std::string& filenName, const std::string& id, sound_type type)
{
	if (type==sound_type::SOUND_MUSIC)
	{
		Mix_Music* pMusic = Mix_LoadMUS(filenName.c_str());
		if (pMusic==0)
		{
			std::cout << "Could not Load Music" << Mix_GetError() << std::endl;
			return false;
		}
		m_music[id] = pMusic;
		return true;
	}
	else if (type==sound_type::SOUND_SFX)
	{
		Mix_Chunk* pChunk = Mix_LoadWAV(filenName.c_str());
		if (pChunk == 0)
		{
			std::cout << "Could not Load Music" << Mix_GetError() << std::endl;
			return false;
		}
		m_sfxs[id] = pChunk;
		return true;
	}
	return false;
}

void SoundManager::PlaySound(const std::string& id, int Loop)
{
	Mix_PlayChannel(-1, m_sfxs[id], Loop);
}

void SoundManager::PlayMusic(const std::string& id, int Loop)
{
	Mix_PlayMusic(m_music[id],Loop);
}

