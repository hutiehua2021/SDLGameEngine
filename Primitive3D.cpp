#include "Primitive3D.h"

char* Get_Line_PLG(char* buffer, int maxlength, FILE* fp)
{
	//这个函数帮助获取文件中一行的内容

	int index = 0;  // 定义 索引
	int length = 0; // 定义 长度

	// 进入循环
	while (1)
	{
		// 读取一行的内容
		if (!fgets(buffer, maxlength, fp))
			return(NULL);

		// 跳过开头的空白
		for (length = strlen(buffer), index = 0; isspace(buffer[index]); index++);

		// 检测如果是空行或者开头是注释，那么就跳过
		if (index >= length || buffer[index] == '#')
			continue;

		// 获取有效的一行
		return(&buffer[index]);
	}

}
///////////////////////////////////////////////////////////////

float Compute_OBJECT4DV1_Radius(OBJECT4DV1_PTR obj)
{
	// 这个函数用来计算最大半径，作为包围球的半径

	// 每次计算前都重置数据
	obj->avg_radius = 0;
	obj->max_radius = 0;

	// 循环每个顶点计算最大距离
	for (int vertex = 0; vertex < obj->num_vertices; vertex++)
	{
		// 更新每个顶点之间的距离
		float dist_to_vertex =
			sqrt(obj->vlist_local[vertex].x * obj->vlist_local[vertex].x +
				obj->vlist_local[vertex].y * obj->vlist_local[vertex].y +
				obj->vlist_local[vertex].z * obj->vlist_local[vertex].z);

		// 计算总距离
		obj->avg_radius += dist_to_vertex;

		// 判断，是否有最大的距离，如果有则更新最大距离
		if (dist_to_vertex > obj->max_radius)
			obj->max_radius = dist_to_vertex;

	}

// 最后，计算平均半径
	obj->avg_radius /= obj->num_vertices;

	// 返回最大半径
	return(obj->max_radius);

}

///////////////////////////////////////////////////////////////

int Load_OBJECT4DV1_PLG(OBJECT4DV1_PTR obj, // 物体指针
	const char* filename,     // 文件名
	VECTOR4D_PTR scale,     // 初始缩放
	VECTOR4D_PTR pos,       // 初始位置
	VECTOR4D_PTR rot)       // 初始旋转
{
	// 这个函数从硬盘中加载数据，并根据传入的缩放，位置，旋转设置顶点信息，并
	//将其保存在对象中

	FILE* fp;          // 文件指针
	char buffer[256];  // 数据缓冲区

	char* token_string;  // 有效文件内容指针

	// Step 1: 清空对象数据，以防有数据残留
	memset(obj, 0, sizeof(OBJECT4DV1));

	// 设置对象属性，保持可见性
	obj->state = OBJECT4DV1_STATE_ACTIVE | OBJECT4DV1_STATE_VISIBLE;

	// 设置世界坐标
	obj->world_pos.x = pos->x;
	obj->world_pos.y = pos->y;
	obj->world_pos.z = pos->z;
	obj->world_pos.w = pos->w;

	// Step 2: 打开文件以待读取
	if (!(fp = fopen(filename, "r")))
	{
		Write_Error("Couldn't open PLG file %s.", filename);
		return(0);
	} // end if

 // Step 3: 获取有效的文件内容以待对象分析（第一行） tri 8 12  
	if (!(token_string = Get_Line_PLG(buffer, 255, fp)))
	{
		Write_Error("PLG file error with file %s (object descriptor invalid).", filename);
		return(0);
	} // end if

	Write_Error("Object Descriptor: %s", token_string);

	// 将内容存入对象中
	sscanf(token_string, "%s %d %d", obj->name, &obj->num_vertices, &obj->num_polys);

	// Step 4: 加载顶点列表
	for (int vertex = 0; vertex < obj->num_vertices; vertex++)
	{
		// 获取下一行，顶点信息
		if (!(token_string = Get_Line_PLG(buffer, 255, fp)))
		{
			Write_Error("PLG file error with file %s (vertex list invalid).", filename);
			return(0);
		} // end if

  // 输出顶点信息到对象中
		sscanf(token_string, "%f %f %f", &obj->vlist_local[vertex].x,
			&obj->vlist_local[vertex].y,
			&obj->vlist_local[vertex].z);
		obj->vlist_local[vertex].w = 1;

		// 缩放顶点
		obj->vlist_local[vertex].x *= scale->x;
		obj->vlist_local[vertex].y *= scale->y;
		obj->vlist_local[vertex].z *= scale->z;

		Write_Error("\nVertex %d = %f, %f, %f, %f", vertex,
			obj->vlist_local[vertex].x,
			obj->vlist_local[vertex].y,
			obj->vlist_local[vertex].z,
			obj->vlist_local[vertex].w);

	}

// 计算平均半径和最大半径
	Compute_OBJECT4DV1_Radius(obj);

	Write_Error("\nObject average radius = %f, max radius = %f",
		obj->avg_radius, obj->max_radius);
	//0xd0f0 3 2 1 0 
	int poly_surface_desc = 0; // PLG/PLX 描述符
	int poly_num_verts = 0; //每个多边形顶点（假定总是3个）
	char tmp_string[8];       

	// Step 5: 加载多边形
	for (int poly = 0; poly < obj->num_polys; poly++)
	{
		// 获取多边形描述符
		if (!(token_string = Get_Line_PLG(buffer, 255, fp)))
		{
			Write_Error("PLG file error with file %s (polygon descriptor invalid).", filename);
			return(0);
		}

		Write_Error("\nPolygon %d:", poly);

		// 我们只读取多边形列表前四项，包括多边形顶点数和3个顶点数据
		//因为我们只绘制三角形
		sscanf(token_string, "%s %d %d %d %d", tmp_string,
			&poly_num_verts,
			&obj->plist[poly].vert[0],
			&obj->plist[poly].vert[1],
			&obj->plist[poly].vert[2]);

		if (tmp_string[0] == '0' && toupper(tmp_string[1]) == 'X')
			sscanf(tmp_string, "%x", &poly_surface_desc);
		else
			poly_surface_desc = atoi(tmp_string);

		//多余的，可以是NULL
		obj->plist[poly].vlist = obj->vlist_local;

		Write_Error("\nSurface Desc = 0x%.4x, num_verts = %d, vert_indices [%d, %d, %d]",
			poly_surface_desc,
			poly_num_verts,
			obj->plist[poly].vert[0],
			obj->plist[poly].vert[1],
			obj->plist[poly].vert[2]);

		//对多边形字段进行分析
		//双面
		if ((poly_surface_desc & PLX_2SIDED_FLAG))
		{
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_2SIDED);
			Write_Error("\n2 sided.");
		}
		else
		{
			//单面
			Write_Error("\n1 sided.");
		} 

		if ((poly_surface_desc & PLX_COLOR_MODE_RGB_FLAG))
		{
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_RGB16);

			int red = ((poly_surface_desc & 0x0f00) >> 8);
			int green = ((poly_surface_desc & 0x00f0) >> 4);
			int blue = (poly_surface_desc & 0x000f);

			obj->plist[poly].color.x = (red<<4)+(0xf);
			obj->plist[poly].color.y = (green<<4) + (0xf);
			obj->plist[poly].color.z = (blue<<4) + (0xf);
			Write_Error("\nRGB color = [%d, %d, %d]", red, green, blue);
		} 
		else
		{
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_8BITCOLOR);

			int red = ((poly_surface_desc & 0x0f00) >> 8);
			int green = ((poly_surface_desc & 0x00f0) >> 4);
			int blue = (poly_surface_desc & 0x000f);

			obj->plist[poly].color.x = (red<<4) + (0xf);
			obj->plist[poly].color.y = (green<<4) + (0xf);
			obj->plist[poly].color.z = (blue<<4) + (0xf);
			Write_Error("\nRGB color = [%d, %d, %d]", red, green, blue);

		}

		int shade_mode = (poly_surface_desc & PLX_SHADE_MODE_MASK);

		switch (shade_mode)
		{
		case PLX_SHADE_MODE_PURE_FLAG: {
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_SHADE_MODE_PURE);
			Write_Error("\nShade mode = pure");
		} break;

		case PLX_SHADE_MODE_FLAT_FLAG: {
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_SHADE_MODE_FLAT);
			Write_Error("\nShade mode = flat");

		} break;

		case PLX_SHADE_MODE_GOURAUD_FLAG: {
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_SHADE_MODE_GOURAUD);
			Write_Error("\nShade mode = gouraud");
		} break;

		case PLX_SHADE_MODE_PHONG_FLAG: {
			SET_BIT(obj->plist[poly].attr, POLY4DV1_ATTR_SHADE_MODE_PHONG);
			Write_Error("\nShade mode = phong");
		} break;

		default: break;
		} 

		obj->plist[poly].state = POLY4DV1_STATE_ACTIVE;

	}

// 关闭文件
	fclose(fp);

	// 返回成功
	return(1);

}
//////////////////////////////////////////////////////////////
void Init_CAM4DV1(CAM4DV1_PTR cam, int cam_attr, POINT4D_PTR cam_pos, POINT4D_PTR cam_target, float near_clip_z, float far_clip_z, float fov, float viewport_width, float viewport_height)
{
	cam->attr = cam_attr;              // 相机属性

	VECTOR4D_COPY(&cam->pos, cam_pos); // 位置

	//UVN
	VECTOR4D_INITXYZ(&cam->u, 1, 0, 0);  // +x
	VECTOR4D_INITXYZ(&cam->v, 0, 1, 0);  // +y
	VECTOR4D_INITXYZ(&cam->n, 0, 0, 1);  // +z      
	VECTOR4D_COPY(&cam->up, &cam->v);	// 设定欧拉相机上方位置
	

	if (cam_target != NULL)
	{
		VECTOR4D_COPY(&cam->target, cam_target); // UVN 目标
		VECTOR4D_Sub(&cam->target, &cam->pos, &cam->front);	//计算前进方向
		VECTOR4D_Normalize(&cam->front);		//归一化
	}
	else
		VECTOR4D_ZERO(&cam->target);

	cam->near_clip_z = near_clip_z;     // 近平面
	cam->far_clip_z = far_clip_z;      // 远平面

	cam->viewport_width = viewport_width;   // 定义视角宽高
	cam->viewport_height = viewport_height;

	cam->viewport_center_x = (viewport_width - 1) / 2; // 视平面中心
	cam->viewport_center_y = (viewport_height - 1) / 2;

	cam->aspect_ratio = (float)viewport_width / (float)viewport_height;	//设置宽高比

	// 初始化相机，投影，屏幕矩阵
	MAT_IDENTITY_4X4(&cam->mcam);
	MAT_IDENTITY_4X4(&cam->mper);
	MAT_IDENTITY_4X4(&cam->mscr);

	cam->fov = DEG_TO_RAD(fov);

	// 设置视平面大小
	cam->viewplane_width = 2.0;
	cam->viewplane_height = 2.0 / cam->aspect_ratio;

	float tan_fov_div2 = tan(DEG_TO_RAD(fov / 2));

	cam->view_dist = (0.5) * (cam->viewplane_width) * tan_fov_div2;

	// 视角为90度，这将很简单
	if (fov == 90.0)
	{
		// 设置视锥
		POINT3D pt_origin; 
		VECTOR3D_INITXYZ(&pt_origin, 0, 0, 0);

		VECTOR3D vn; // 平面法线

		// 右裁剪平面
		VECTOR3D_INITXYZ(&vn, 1, 0, -1); // x=z 
		PLANE3D_Init(&cam->rt_clip_plane, &pt_origin, &vn, 1);

		// 左裁剪平面
		VECTOR3D_INITXYZ(&vn, -1, 0, -1); // -x=z
		PLANE3D_Init(&cam->lt_clip_plane, &pt_origin, &vn, 1);

		// 上裁剪平面
		VECTOR3D_INITXYZ(&vn, 0, 1, -1); // y=z 
		PLANE3D_Init(&cam->tp_clip_plane, &pt_origin, &vn, 1);

		// 下裁剪平面
		VECTOR3D_INITXYZ(&vn, 0, -1, -1); // -y=z 
		PLANE3D_Init(&cam->bt_clip_plane, &pt_origin, &vn, 1);
	}
	else
	{
		POINT3D pt_origin;
		VECTOR3D_INITXYZ(&pt_origin, 0, 0, 0);

		VECTOR3D vn;

		VECTOR3D_INITXYZ(&vn, cam->view_dist, 0, -cam->viewplane_width / 2.0);
		PLANE3D_Init(&cam->rt_clip_plane, &pt_origin, &vn, 1);

		VECTOR3D_INITXYZ(&vn, -cam->view_dist, 0, -cam->viewplane_width / 2.0);
		PLANE3D_Init(&cam->lt_clip_plane, &pt_origin, &vn, 1);

		VECTOR3D_INITXYZ(&vn, 0, cam->view_dist, -cam->viewplane_width / 2.0);
		PLANE3D_Init(&cam->tp_clip_plane, &pt_origin, &vn, 1);

		VECTOR3D_INITXYZ(&vn, 0, -cam->view_dist, -cam->viewplane_width / 2.0);
		PLANE3D_Init(&cam->bt_clip_plane, &pt_origin, &vn, 1);
	}

}
void Transfrom_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, MATRIX4X4_PTR mt, int coord_select)
{
	switch (coord_select)
	{
	case TRANSFORM_LOCAL_ONLY:	//对变换前的顶点进行变换
	{
		for (int poly = 0; poly < rend_list->num_polys; poly++)
		{
			POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];
			//检测可见性
			if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
				(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
				(curr_poly->state & POLY4DV1_STATE_BACKFACE))
				continue; 
			for (int vertex = 0; vertex < 3; vertex++)
			{
				POINT4D presult;
				Mat_Mul_VECTOR4D_4X4(&curr_poly->vlist[vertex], mt, &presult);
				VECTOR4D_COPY(&curr_poly->vlist[vertex], &presult);
			}
		}
	} break;
	case TRANSFORM_TRANS_ONLY:
	{
		for (int poly = 0; poly < rend_list->num_polys; poly++)
		{
			POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];
			//检测可见性
			if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
				(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
				(curr_poly->state & POLY4DV1_STATE_BACKFACE))
				continue;

			for (int vertex = 0; vertex < 3; vertex++)
			{
				POINT4D presult;
				Mat_Mul_VECTOR4D_4X4(&curr_poly->tvlist[vertex], mt, &presult);
				VECTOR4D_COPY(&curr_poly->tvlist[vertex], &presult);
			}
		}
	} break;
	case TRANSFORM_LOCAL_TO_TRANS:
	{
		for (int poly = 0; poly < rend_list->num_polys; poly++)
		{
			POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];
			if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
				(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
				(curr_poly->state & POLY4DV1_STATE_BACKFACE))
				continue;
			for (int vertex = 0; vertex < 3; vertex++)
			{
				Mat_Mul_VECTOR4D_4X4(&curr_poly->vlist[vertex], mt, &curr_poly->tvlist[vertex]);
			}
		} 
	} break;
	default: break;
	}
}

void Transform_OBJECT4DV1(OBJECT4DV1_PTR obj, MATRIX4X4_PTR mt, int coord_select, int transform_basis)
{
	switch (coord_select)
	{
	case TRANSFORM_LOCAL_ONLY:
	{
		for (int vertex = 0; vertex < obj->num_vertices; vertex++)
		{
			POINT4D presult;
			Mat_Mul_VECTOR4D_4X4(&obj->vlist_local[vertex], mt, &presult);
			VECTOR4D_COPY(&obj->vlist_local[vertex], &presult);
		}
	} break;
	case TRANSFORM_TRANS_ONLY:
	{
		for (int vertex = 0; vertex < obj->num_vertices; vertex++)
		{
			POINT4D presult;
			Mat_Mul_VECTOR4D_4X4(&obj->vlist_trans[vertex], mt, &presult);
			VECTOR4D_COPY(&obj->vlist_trans[vertex], &presult);
		}
	} break;
	case TRANSFORM_LOCAL_TO_TRANS:
	{
		for (int vertex = 0; vertex < obj->num_vertices; vertex++)
		{
			POINT4D presult;
			Mat_Mul_VECTOR4D_4X4(&obj->vlist_local[vertex], mt, &obj->vlist_trans[vertex]);
		} 
	} break;
	default: break;
	}
}

void Build_Model_To_World_Mat4(VECTOR4D_PTR vpos, MATRIX4X4_PTR m)
{
	Mat_Init_4X4(m, 1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		vpos->x, vpos->y, vpos->z, 1);
}

int Check_CVV(VECTOR4D_PTR v)
{
	float w = v->w;	//因为没有归一化，所以这里用w来表示边界
	int check = 0;
	if (v->z < 0.0f) check |= 1;
	if (v->z > w) check |= 2;
	if (v->x < -w) check |= 4;
	if (v->x > w) check |= 8;
	if (v->y < -w) check |= 16;
	if (v->y > w) check |= 32;
	return check;
}

void Cull_CVV_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list)
{
	for (int poly = 0; poly < rend_list->num_polys; poly++)
	{
		POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];

		if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->attr & POLY4DV1_ATTR_2SIDED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue;

		if (Check_CVV(&curr_poly->tvlist[0]))
			SET_BIT(curr_poly->state, POLY4DV1_STATE_CLIPPED);
		if (Check_CVV(&curr_poly->tvlist[1]))
			SET_BIT(curr_poly->state, POLY4DV1_STATE_CLIPPED);
		if (Check_CVV(&curr_poly->tvlist[2]))
			SET_BIT(curr_poly->state, POLY4DV1_STATE_CLIPPED);

	}
}

int Cull_OBJECT4DV1(OBJECT4DV1_PTR obj, CAM4DV1_PTR cam,MATRIX4X4_PTR m, int cull_flags)
{
// step 1: 将物体的世界坐标变换到相机坐标系中

	POINT4D sphere_pos; // 保存包围球

	Mat_Mul_VECTOR4D_4X4(&obj->world_pos, m, &sphere_pos);

	// step 2:  根据标志判断如何裁剪
	if (cull_flags & CULL_OBJECT_Z_PLANE)	//裁剪z平面，远近
	{
		if (((sphere_pos.z - obj->max_radius) > cam->far_clip_z) ||
			((sphere_pos.z + obj->max_radius) < cam->near_clip_z))
		{
			SET_BIT(obj->state, OBJECT4DV1_STATE_CULLED);
			return(1);
		}
	} 

	if (cull_flags & CULL_OBJECT_X_PLANE)
	{
		//x - z
		//x = k*z
		//k = 0.5 *  cam->viewplane_width / cam->view_dist
		float z_test = (0.5) * cam->viewplane_width * sphere_pos.z / cam->view_dist;

		if (((sphere_pos.x - obj->max_radius) > z_test) || // right 
			((sphere_pos.x + obj->max_radius) < -z_test))  // left
		{
			SET_BIT(obj->state, OBJECT4DV1_STATE_CULLED);
			return(1);
		} 
	}

	if (cull_flags & CULL_OBJECT_Y_PLANE)
	{
		// y - z
		// y = k*z
		//k = 0.5 *  cam->viewplane_width / cam->view_dist
		float z_test = (0.5) * cam->viewplane_height * sphere_pos.z / cam->view_dist;

		if (((sphere_pos.y - obj->max_radius) > z_test) || // top 
			((sphere_pos.y + obj->max_radius) < -z_test))  // bottom
		{
			SET_BIT(obj->state, OBJECT4DV1_STATE_CULLED);
			return(1);
		} 
	} 
	return(0);

}

void Reset_OBJECT4DV1(OBJECT4DV1_PTR obj)
{
	RESET_BIT(obj->state, OBJECT4DV1_STATE_CULLED);

	for (int poly = 0; poly < obj->num_polys; poly++)
	{
		POLY4DV1_PTR curr_poly = &obj->plist[poly];

		if (!(curr_poly->state & POLY4DV1_STATE_ACTIVE))
			continue; 

		RESET_BIT(curr_poly->state, POLY4DV1_STATE_CLIPPED);
		RESET_BIT(curr_poly->state, POLY4DV1_STATE_BACKFACE);
	}
}

void Reset_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list)
{
	rend_list->num_polys = 0;
}

int Insert_POLY4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, POLY4DV1_PTR poly)
{

// step 0: 检测队列是否已经满了
	if (rend_list->num_polys >= RENDERLIST4DV1_MAX_POLYS)
		return(0);

	// step 1: 将多边形顶点信息复制到渲染队列中

	// 将下一个指针指向下一个顶点信息
	rend_list->poly_ptrs[rend_list->num_polys] = &rend_list->poly_data[rend_list->num_polys];

	// 复制属性，状态，颜色
	rend_list->poly_data[rend_list->num_polys].state = poly->state;
	rend_list->poly_data[rend_list->num_polys].attr = poly->attr;
	rend_list->poly_data[rend_list->num_polys].color = poly->color;

	// 现在将顶点复制到渲染队列中，现在是复制3个顶点
	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].tvlist[0],
		&poly->vlist[poly->vert[0]]);

	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].tvlist[1],
		&poly->vlist[poly->vert[1]]);

	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].tvlist[2],
		&poly->vlist[poly->vert[2]]);

	// 复制到原始顶点中
	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].vlist[0],
		&poly->vlist[poly->vert[0]]);

	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].vlist[1],
		&poly->vlist[poly->vert[1]]);

	VECTOR4D_COPY(&rend_list->poly_data[rend_list->num_polys].vlist[2],
		&poly->vlist[poly->vert[2]]);

	// 现在顶点被放置在队列中，接下来我们设置指针（用一个链表表示多边形渲染顺序）

	// 如果是第一个多边形
	if (rend_list->num_polys == 0)
	{
		rend_list->poly_data[0].next = NULL;
		rend_list->poly_data[0].prev = NULL;
	}
	else
	{
		rend_list->poly_data[rend_list->num_polys].next = NULL;
		rend_list->poly_data[rend_list->num_polys].prev =
			&rend_list->poly_data[rend_list->num_polys - 1];

		rend_list->poly_data[rend_list->num_polys - 1].next =
			&rend_list->poly_data[rend_list->num_polys];
	}
	rend_list->num_polys++;
	return(1);

}

int Insert_POLYF4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, POLYF4DV1_PTR poly)
{
	//注意POLYF4DV1_PTR本身是渲染队列的一个节点（只不过指向都为空）

// step 0: 检测是否满队列了
	if (rend_list->num_polys >= RENDERLIST4DV1_MAX_POLYS)
		return(0);

	// step 1: 设置顶点指向
	rend_list->poly_ptrs[rend_list->num_polys] = &rend_list->poly_data[rend_list->num_polys];

	// 直接将结构体复制到渲染队列中
	memcpy((void*)&rend_list->poly_data[rend_list->num_polys], (void*)poly, sizeof(POLYF4DV1));

	//设置链表链接
	if (rend_list->num_polys == 0)
	{
		rend_list->poly_data[0].next = NULL;
		rend_list->poly_data[0].prev = NULL;
	} 
	else
	{
		rend_list->poly_data[rend_list->num_polys].next = NULL;
		rend_list->poly_data[rend_list->num_polys].prev =
			&rend_list->poly_data[rend_list->num_polys - 1];

		rend_list->poly_data[rend_list->num_polys - 1].next =
			&rend_list->poly_data[rend_list->num_polys];
	}
	rend_list->num_polys++;

	return(1);

}

int Insert_OBJECT4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, OBJECT4DV1_PTR obj, int insert_local, int lighting_on)
{
// 检测对象是否可见
	if (!(obj->state & OBJECT4DV1_STATE_ACTIVE) ||
		(obj->state & OBJECT4DV1_STATE_CULLED) ||
		!(obj->state & OBJECT4DV1_STATE_VISIBLE))
		return(0);

	// 拆分对象的多边形列表
	for (int poly = 0; poly < obj->num_polys; poly++)
	{
		POLY4DV1_PTR curr_poly = &obj->plist[poly];

		// 检测多边形是否可见
		if (!(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue;

			// 保存局部坐标，如果变换要将局部坐标插入，那么由于取得的curr_poly
			// 是指针对象而且Insert_POLY4DV1_RENDERLIST4DV1（）函数
			// 读取vlist的值，所以用vlist_old缓存数据，在完成操作后归还数据
		VECTOR3D color;
		if (lighting_on)
		{
			color  = curr_poly->color;
		}
		POINT4D_PTR vlist_old = curr_poly->vlist;
		if (insert_local)
			curr_poly->vlist = obj->vlist_local;
		else
			curr_poly->vlist = obj->vlist_trans;
		// 现在插入数据 (即将多边形vlist中的数据拷贝到渲染列表poly_data中)
		if (!Insert_POLY4DV1_RENDERLIST4DV1(rend_list, curr_poly))
		{
			curr_poly->vlist = vlist_old;
			return(0);
		} 
		if (lighting_on)
		{
			curr_poly->color = color;
		}
		curr_poly->vlist = vlist_old;
	}
	return(1);
}

void Remove_BackFaces_OBJECT4DV1(OBJECT4DV1_PTR obj, CAM4DV1_PTR cam)
{
	if (obj->state & OBJECT4DV1_STATE_CULLED)
		return;

	// 遍历对象中的多边形
	for (int poly = 0; poly < obj->num_polys; poly++)
	{
		POLY4DV1_PTR curr_poly = &obj->plist[poly];

		// 检测多边形的属性，未激活，双面，和已经被剔除的不需要进行重复计算
		if (!(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->attr & POLY4DV1_ATTR_2SIDED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue; // move onto next poly

		 // 现在由于多边形是3个顶点，获取他们
		int vindex_0 = curr_poly->vert[0];
		int vindex_1 = curr_poly->vert[1];
		int vindex_2 = curr_poly->vert[2];

		// 分别计算向量，按顺时针方式
		VECTOR4D u, v, n;

		// u, v
		VECTOR4D_Build(&obj->vlist_trans[vindex_0], &obj->vlist_trans[vindex_1], &u);
		VECTOR4D_Build(&obj->vlist_trans[vindex_0], &obj->vlist_trans[vindex_2], &v);

		// 做×乘
		VECTOR4D_Cross(&u, &v, &n);

		// 获取视点和向量的家教
		VECTOR4D view;
		VECTOR4D_Build(&obj->vlist_trans[vindex_0], &cam->pos, &view);

		float dp = VECTOR4D_Dot(&n, &view);

		// if the sign is > 0 then visible, 0 = scathing, < 0 invisible
		if (dp <= 0.0)
			SET_BIT(curr_poly->state, POLY4DV1_STATE_BACKFACE);
	} 

}

void Remove_BackFaces_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, CAM4DV1_PTR cam)
{
	for (int poly = 0; poly < rend_list->num_polys; poly++)
	{
		POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];

		if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->attr & POLY4DV1_ATTR_2SIDED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue;

		VECTOR4D u, v, n;

		VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[1], &u);
		VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[2], &v);

		VECTOR4D_Cross(&u, &v, &n);

		VECTOR4D view;
		VECTOR4D_Build(&curr_poly->tvlist[0], &cam->pos, &view);

		float dp = VECTOR4D_Dot(&n, &view);

		if (dp <= 0.0)
			SET_BIT(curr_poly->state, POLY4DV1_STATE_BACKFACE);

	}
}


void Build_Camera_To_Perspective_Matrix4(CAM4DV1_PTR cam,MATRIX4X4_PTR m)
{
	float fax = 1.0f / (float)tan(cam->fov * 0.5f);
	MAT_ZERO_4X4(m)
	m->M[0][0] = (float)(fax / cam->aspect_ratio);
	m->M[1][1] = (float)(fax);
	m->M[2][2] = cam->far_clip_z / (cam->far_clip_z - cam->near_clip_z);
	m->M[3][2] = -cam->near_clip_z * cam->far_clip_z / (cam->far_clip_z - cam->near_clip_z);
	m->M[2][3] = 1;
}

void View_To_Screen_OBJECT4DV1(OBJECT4DV1_PTR obj, CAM4DV1_PTR cam)
{
	for (int vertex = 0; vertex < obj->num_vertices; vertex++)
	{
		float rhw = 1.0f / obj->vlist_trans[vertex].w;
		obj->vlist_trans[vertex].x = (obj->vlist_trans[vertex].x * rhw + 1.0f) * 0.5f * cam->viewport_width;
		obj->vlist_trans[vertex].y = (1.0f - obj->vlist_trans[vertex].y * rhw) * 0.5f * cam->viewport_height;
		obj->vlist_trans[vertex].z = obj->vlist_trans[vertex].z * rhw;
		obj->vlist_trans[vertex].w = 1.0f;
	}
}


void View_To_Screen_RENDERLISR4DV1(RENDERLIST4DV1_PTR rend_list, CAM4DV1_PTR cam)
{
	for (int poly = 0; poly < rend_list->num_polys; poly++)
	{

		POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];

		if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue;

		for (int vertex = 0; vertex < 3; vertex++)
		{
			float rhw = 1.0f / curr_poly->tvlist[vertex].w;
			float test = curr_poly->tvlist[vertex].x * rhw;
			curr_poly->tvlist[vertex].x = (curr_poly->tvlist[vertex].x * rhw + 1.0f) * 0.5 * cam->viewport_width;
			curr_poly->tvlist[vertex].y = (1.0f - curr_poly->tvlist[vertex].y * rhw) * 0.5 * cam->viewport_height;
			curr_poly->tvlist[vertex].z = curr_poly->tvlist[vertex].z * rhw;
			curr_poly->vlist[vertex].w = 1.0f;
		}
	}
}

void Draw_OBJECT4DV1_Wrie(OBJECT4DV1_PTR obj, Primitive2D* primitive)
{
	for (int poly=0;poly<obj->num_polys;++poly)
	{
		if (!(obj->plist[poly].state & POLY4DV1_STATE_ACTIVE)||
			(obj->plist[poly].state & POLY4DV1_STATE_CLIPPED)||
			(obj->plist[poly].state & POLY4DV1_STATE_BACKFACE))
		{
			continue;
		}
		int vindex_0 = obj->plist[poly].vert[0];
		int vindex_1 = obj->plist[poly].vert[1];
		int vindex_2 = obj->plist[poly].vert[2];

		primitive->ClipDrawLine(vec2(obj->vlist_trans[vindex_0].x, obj->vlist_trans[vindex_0].y), vec2(obj->vlist_trans[vindex_1].x, obj->vlist_trans[vindex_1].y), obj->plist[poly].color);

		primitive->ClipDrawLine(vec2(obj->vlist_trans[vindex_1].x, obj->vlist_trans[vindex_1].y), vec2(obj->vlist_trans[vindex_2].x, obj->vlist_trans[vindex_2].y), obj->plist[poly].color);

		primitive->ClipDrawLine(vec2(obj->vlist_trans[vindex_2].x, obj->vlist_trans[vindex_2].y), vec2(obj->vlist_trans[vindex_0].x, obj->vlist_trans[vindex_0].y), obj->plist[poly].color);
	}
}

void Draw_RENDERLIST4DV1_Wrie(RENDERLIST4DV1_PTR rend_list, Primitive2D* primitive)
{
	for (int poly =0;poly<rend_list->num_polys;++poly)
	{
		//判断是否有效
		if ((rend_list->poly_ptrs[poly] == NULL) ||
			!(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_ACTIVE) ||
			(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_CLIPPED) ||
			(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_BACKFACE))
		{
			continue;
		}

		primitive->ClipDrawLine(vec2(rend_list->poly_ptrs[poly]->tvlist[0].x, rend_list->poly_ptrs[poly]->tvlist[0].y), vec2(rend_list->poly_ptrs[poly]->tvlist[1].x, rend_list->poly_ptrs[poly]->tvlist[1].y), rend_list->poly_ptrs[poly]->color);

		primitive->ClipDrawLine(vec2(rend_list->poly_ptrs[poly]->tvlist[1].x, rend_list->poly_ptrs[poly]->tvlist[1].y), vec2(rend_list->poly_ptrs[poly]->tvlist[2].x, rend_list->poly_ptrs[poly]->tvlist[2].y), rend_list->poly_ptrs[poly]->color);

		primitive->ClipDrawLine(vec2(rend_list->poly_ptrs[poly]->tvlist[2].x, rend_list->poly_ptrs[poly]->tvlist[2].y), vec2(rend_list->poly_ptrs[poly]->tvlist[0].x, rend_list->poly_ptrs[poly]->tvlist[0].y), rend_list->poly_ptrs[poly]->color);
	}
}

void Draw_OBJECT4DV1_Solid(OBJECT4DV1_PTR obj, Primitive2D* primitive)
{
	for (int poly = 0; poly < obj->num_polys; ++poly)
	{
		if (!(obj->plist[poly].state & POLY4DV1_STATE_ACTIVE) ||
			(obj->plist[poly].state & POLY4DV1_STATE_CLIPPED) ||
			(obj->plist[poly].state & POLY4DV1_STATE_BACKFACE))
		{
			continue;
		}
		int vindex_0 = obj->plist[poly].vert[0];
		int vindex_1 = obj->plist[poly].vert[1];
		int vindex_2 = obj->plist[poly].vert[2];

		primitive->DrawTriangle2D(
			VECTOR2D(obj->vlist_trans[vindex_0].x, obj->vlist_trans[vindex_0].y),
			VECTOR2D(obj->vlist_trans[vindex_1].x, obj->vlist_trans[vindex_1].y),
			VECTOR2D(obj->vlist_trans[vindex_2].x, obj->vlist_trans[vindex_2].y),
			obj->plist[poly].color
		);
	}
}

void Draw_RENDERLIST4DV1_Solid(RENDERLIST4DV1_PTR rend_list, Primitive2D* primitive)
{
	for (int poly = 0; poly < rend_list->num_polys; ++poly)
	{
		//判断是否有效
		if ((rend_list->poly_ptrs[poly] == NULL) ||
			!(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_ACTIVE) ||
			(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_CLIPPED) ||
			(rend_list->poly_ptrs[poly]->state & POLY4DV1_STATE_BACKFACE))
		{
			continue;
		}
		primitive->DrawTriangle2D(
			VECTOR2D(rend_list->poly_ptrs[poly]->tvlist[0].x,rend_list->poly_ptrs[poly]->tvlist[0].y), 
			VECTOR2D(rend_list->poly_ptrs[poly]->tvlist[1].x, rend_list->poly_ptrs[poly]->tvlist[1].y),
			VECTOR2D(rend_list->poly_ptrs[poly]->tvlist[2].x, rend_list->poly_ptrs[poly]->tvlist[2].y),
			rend_list->poly_ptrs[poly]->color);
	}
}

void Sort_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, int sort_method)
{
	switch (sort_method)
	{
	case SORT_POLYLIST_AVGZ:
		qsort((void*)rend_list->poly_ptrs, rend_list->num_polys, sizeof(POLY4DV1_PTR), Compare_AvgZ_POLYF4DV1);
		break;
	case SORT_POLYLIST_NEARZ:
		qsort((void*)rend_list->poly_ptrs, rend_list->num_polys, sizeof(POLY4DV1_PTR),Compare_NEARZ_POLYF4DV1);
		break;
	case SORT_POLTLIST_FARZ:
		qsort((void*)rend_list->poly_ptrs, rend_list->num_polys, sizeof(POLY4DV1_PTR), Compare_FARZ_POLYF4DV1);
		break;
	default:
		break;
	}
}

int Compare_AvgZ_POLYF4DV1(const void* arg1, const void* arg2)
{
	float z1, z2;	//用来表示两个多边形的平均z值
	POLYF4DV1_PTR poly_1, poly_2;	//多边形指针
	//获取多边形
	poly_1 = (*(POLYF4DV1_PTR*)(arg1));
	poly_2 = (*(POLYF4DV1_PTR*)(arg2));
	//计算第一个多边形平均z值
	z1 = (float)0.33333 * (poly_1->tvlist[0].z + poly_1->tvlist[1].z + poly_1->tvlist[2].z);
	//计算第二个多边形平均z值
	z2 = (float)0.33333 * (poly_2->tvlist[0].z + poly_2->tvlist[1].z + poly_2->tvlist[2].z);
	//将z1和z2进行比较，
	if (z1 > z2)
	{
		return(-1);
	}
	else if (z1 < z2)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}

int Compare_FARZ_POLYF4DV1(const void* arg1, const void* arg2)
{
	float z1, z2, m;	//用来表示两个多边形的平均z值
	POLYF4DV1_PTR poly_1, poly_2;	//多边形指针
	//获取多边形
	poly_1 = (*(POLYF4DV1_PTR*)(arg1));
	poly_2 = (*(POLYF4DV1_PTR*)(arg2));

	m = poly_1->tvlist[0].z > poly_1->tvlist[1].z ? poly_1->tvlist[0].z : poly_1->tvlist[1].z;
	z1 = m > poly_1->tvlist[2].z ? m : poly_1->tvlist[2].z;

	m = poly_2->tvlist[0].z > poly_2->tvlist[1].z ? poly_2->tvlist[0].z : poly_2->tvlist[1].z;
	z2 = m > poly_2->tvlist[2].z ? m : poly_2->tvlist[2].z;

	//将z1和z2进行比较，
	if (z1 > z2)
	{
		return(-1);
	}
	else if (z1 < z2)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}

int Compare_NEARZ_POLYF4DV1(const void* arg1, const void* arg2)
{
	float z1, z2, m;	//用来表示两个多边形的平均z值
	POLYF4DV1_PTR poly_1, poly_2;	//多边形指针
	//获取多边形
	poly_1 = (*(POLYF4DV1_PTR*)(arg1));
	poly_2 = (*(POLYF4DV1_PTR*)(arg2));

	m = poly_1->tvlist[0].z < poly_1->tvlist[1].z ? poly_1->tvlist[0].z : poly_1->tvlist[1].z;
	z1 = m < poly_1->tvlist[2].z ? m : poly_1->tvlist[2].z;

	m = poly_2->tvlist[0].z < poly_2->tvlist[1].z ? poly_2->tvlist[0].z : poly_2->tvlist[1].z;
	z2 = m < poly_2->tvlist[2].z ? m : poly_2->tvlist[2].z;

	//将z1和z2进行比较，
	if (z1 > z2)
	{
		return(-1);
	}
	else if (z1 < z2)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
