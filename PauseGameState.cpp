#include "PauseGameState.h"
#include "MainMenuState.h"
const std::string PauseGameState::s_pauseID = "PAUSE";
PauseGameState::~PauseGameState()
{
	m_gameObjects.clear();
}
void PauseGameState::update(float dt)
{
	for (auto iter : m_gameObjects)
		iter->Update(dt);
}

void PauseGameState::renderer()
{
	for (auto iter : m_gameObjects)
		iter->Draw();
}

bool PauseGameState::onEnter()
{
	StateParser stateParse;
	stateParse.ParseState("test.xml", s_pauseID, m_gameObjects, m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_pauseToMain);
	m_callbacks.push_back(s_resumePlay);

	SetCallBack(m_callbacks);

	std::cout << "Enter PauseState\n";
	return true;
}

bool PauseGameState::onExit()
{
	for (auto iter : m_gameObjects)
		iter->Clean();
	m_gameObjects.clear();
	for (size_t i = 0; i < m_textureIDList.size(); i++)
	{
		TextureManager::Instance()->ClearFromTextureMap(m_textureIDList[i]);
	}
	m_textureIDList.clear();
	return true;
}

const std::string& PauseGameState::getStateID() const
{
	return s_pauseID;
}

void PauseGameState::SetCallBack(const std::vector<Callback>& callbacks)
{
	for (size_t i = 0; i < m_gameObjects.size(); i++)
	{
		if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
		{
			MenuButton* button = dynamic_cast<MenuButton*>(m_gameObjects[i]);
			button->SetCallBack(callbacks[button->GetCallBackID()]);
		}
	}
}

void PauseGameState::s_pauseToMain()
{
	Game::Instance()->getGameStateMachine()->changeState(new MainMenuState());
}

void PauseGameState::s_resumePlay()
{
	Game::Instance()->getGameStateMachine()->popState();
}
