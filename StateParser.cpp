#include "StateParser.h"
#include "TextureManager.h"
#include "Game.h"
#include "GameObjectFactory.h"
#include <iostream>
bool StateParser::ParseState(const char* fileName, const std::string& stateId, std::vector<GameObject*>& pObjects, std::vector<std::string>& pTextureIds)
{
	TiXmlDocument xmlDoc;

	if (!xmlDoc.LoadFile(fileName))
	{
		std::cerr << xmlDoc.ErrorDesc() << "\n";
		return false;
	}
	//根节点元素
	TiXmlElement* pRoot = xmlDoc.RootElement();
	//状态节点
	TiXmlElement* pStateRoot = 0;
	for (TiXmlElement* e = pRoot->FirstChildElement();e!=NULL;e=e->NextSiblingElement())
	{
		if (e->Value()==stateId)
		{
			pStateRoot = e;
		}
	}
	//贴图节点
	TiXmlElement* pTextureRoot = 0;
	for (TiXmlElement* e = pStateRoot->FirstChildElement();e!=NULL;e=e->NextSiblingElement())
	{
		if (e->Value()==std::string("TEXTURES"))
		{
			pTextureRoot = e;
		}
	}
	//解析贴图节点
	ParseTexture(pTextureRoot, pTextureIds);

	//对象节点
	TiXmlElement* pObjectRoot = 0;
	for (TiXmlElement* e=pStateRoot->FirstChildElement();e!=NULL;e=e->NextSiblingElement())
	{
		if (e->Value()==std::string("OBJECTS"))
		{
			pObjectRoot = e;
		}
	}

	ParseObjects(pObjectRoot, pObjects);

	return true;
}

void StateParser::ParseObjects(TiXmlElement* pStateRoot, std::vector<GameObject*>& pObjects)
{
	for (TiXmlElement* e=pStateRoot->FirstChildElement();e!=NULL;e=e->NextSiblingElement())
	{
		int x, y, width, height, numFrames, callbackId, animSpeed;
		std::string textureID;

		e->Attribute("x", &x);
		e->Attribute("y", &y);
		e->Attribute("width", &width);
		e->Attribute("height", &height);
		e->Attribute("numFrames", &numFrames);
		e->Attribute("callbackID", &callbackId);
		e->Attribute("animSpeed", &animSpeed);

		textureID = e->Attribute("textureID");

		GameObject* pGameObject = GameObjectFactory::Instance()->Create(e->Attribute("type"));
		pGameObject->Load(new LoaderParams(x,y,width,height,textureID,numFrames,callbackId,animSpeed));

		pObjects.push_back(pGameObject);
	}
}

void StateParser::ParseTexture(TiXmlElement* pStateRoot, std::vector<std::string>& pTextureIds)
{
	for (TiXmlElement* e=pStateRoot->FirstChildElement();e!=NULL;e=e->NextSiblingElement())
	{
		std::string filenameAttribute = e->Attribute("filename");
		std::string idAttribute = e->Attribute("ID");
		pTextureIds.push_back(idAttribute);
		
		if (!TextureManager::Instance()->Load(filenameAttribute, idAttribute, Game::Instance()->getRenderer()))
		{
			std::cout << __FILE__ << "\t" << __FUNCTION__ << "\t" << "贴图加载错误\n";
		}
	}
}
