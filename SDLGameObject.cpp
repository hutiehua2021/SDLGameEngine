#include "SDLGameObject.h"
#include "Game.h"
SDLGameObject::SDLGameObject(const LoaderParams* pParams)
	:GameObject(pParams),m_postition(pParams->getX(),pParams->getY()),m_velocity(0,0),m_acceleration(0,0)
{
	m_height = pParams->getHeight();
	m_width = pParams->getWidth();
	m_textureId = pParams->getTextureId();
	m_numFrames = pParams->getNumFrmes();

	m_currentFrame = 1;
	m_currentRow = 1;
}

void SDLGameObject::Draw()
{
	TextureManager::Instance()->DrawFrame(m_textureId, (int)m_postition.x, (int)m_postition.y, m_width, m_height, m_currentRow, m_currentFrame, Game::Instance()->getRenderer());
}

void SDLGameObject::Update(float dt)
{
	m_velocity += m_acceleration;
	m_postition += m_velocity;
}

void SDLGameObject::Clean()
{
}

void SDLGameObject::Load(const LoaderParams* pParams)
{
	m_postition = vec2(pParams->getX(),pParams->getY());
	m_velocity = vec2(0, 0);
	m_acceleration = vec2(0, 0);
	m_width = pParams->getWidth();
	m_height = pParams->getHeight();
	m_textureId = pParams->getTextureId();
	m_currentFrame = 1;
	m_currentRow = 1;
	m_numFrames = pParams->getNumFrmes();
}
