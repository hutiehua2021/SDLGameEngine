#include "Game.h"
#include "Player.h"
#include <iostream>
const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

int main(int argc, char* argv[])
{
	std::cout << "Init Game Engine ***********\n";
	Uint32 frameStart, frameTime;
	if (Game::Instance()->Init("Chapter 1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, false))
	{
		std::cout << "Init Game Engine Success!\n";
		frameStart = SDL_GetTicks();
		while (Game::Instance()->Running())
		{
			Game::Instance()->Render();
			frameTime = SDL_GetTicks() - frameStart;
			Game::Instance()->Update(float(frameTime));
			Game::Instance()->HandleEvent(float(frameTime));
			if (frameTime<DELAY_TIME)
			{
				SDL_Delay((int)(DELAY_TIME - frameTime));
			}
		}
	}
	else
	{
		std::cout << "Init Game Engine Failed\n";
		return -1;
	}
	std::cout << "Game Engine Close...\n";
	Game::Instance()->Clean();
	return 0;
}