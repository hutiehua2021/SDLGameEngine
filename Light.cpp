#include "Light.h"

int Reset_Lights_LIGHTV1(void)
{
	static int first_time = 1;
	memset(Lights, 0, MAX_LIGHTS * sizeof(LIGHTV1));
	//重设光源数（关闭，所以为0）
	num_lights = 0;
	first_time = 0;
	return (1);
}

int Init_Light_LIGHTV1(int index, int _state, int _attr, RGBAV1 _c_ambient, RGBAV1 _c_diffuse, RGBAV1 _c_specular, POINT4D_PTR _pos, VECTOR4D_PTR _dir, float _kc, float _kl, float _kq, float _spot_inner, float _spot_outer, float _pt)
{
	if (index<0 || index>MAX_LIGHTS)	//不在范围内
		return (0);
	//初始化
	Lights[index].state = _state;
	Lights[index].attr = _attr;
	Lights[index].id = index;
	Lights[index].c_ambient = _c_ambient;
	Lights[index].c_diffuse = _c_diffuse;
	Lights[index].c_specular = _c_specular;
	Lights[index].kc = _kc;
	Lights[index].kl = _kl;
	Lights[index].kq = _kq;
	if (_pos)
		VECTOR4D_COPY(&Lights[index].pos, _pos);
	if (_dir)
	{
		VECTOR4D_COPY(&Lights[index].dir, _dir);
		VECTOR4D_Normalize(&Lights[index].dir);
	}
	Lights[index].spot_inner = _spot_inner;
	Lights[index].spot_outer = _spot_outer;

	//返回索引
	return (index);
}

int Light_RENDERLIST4DV1_World(RENDERLIST4DV1_PTR rend_list, CAM4DV1_PTR cam, int max_lights)
{
	//支持固定着色，环境光，漫反射，点光源，无穷远光，聚光灯
	
	float r_base, g_base, b_base;	//源色彩
	float r_sum, g_sum, b_sum;		//总体光照效果
	float nl,dp,i,dist,atten;
	VECTOR4D l;

	for (int poly = 0; poly < rend_list->num_polys; poly++)
	{
		POLYF4DV1_PTR curr_poly = rend_list->poly_ptrs[poly];
		//检测多边形是否被剔除
		if ((curr_poly == NULL) || !(curr_poly->state & POLY4DV1_STATE_ACTIVE) ||
			(curr_poly->state & POLY4DV1_STATE_CLIPPED) ||
			(curr_poly->state & POLY4DV1_STATE_BACKFACE))
			continue;
		//获取初始颜色
		r_base = curr_poly->color.x;
		g_base = curr_poly->color.y;
		b_base = curr_poly->color.z;
		//初始化总体颜色
		r_sum = 0;
		g_sum = 0;
		b_sum = 0;
		if (curr_poly->attr & POLY4DV1_ATTR_SHADE_MODE_FLAT || curr_poly->attr & POLY4DV1_ATTR_SHADE_MODE_GOURAUD)
		{
			//遍历光源
			for (int curr_light = 0; curr_light < max_lights; ++curr_light)
			{
				//判断光源是否开启
				if (!Lights[curr_light].state)
					continue;
				//环境光
				if (Lights[curr_light].attr & LIGHTV1_ATTR_AMBIENT)
				{
					//除265，保证结果在 0 - 255之前
					r_sum += ((Lights[curr_light].c_ambient.r * r_base) / 256);
					g_sum += ((Lights[curr_light].c_ambient.g * g_base) / 256);
					b_sum += ((Lights[curr_light].c_ambient.b * b_base) / 256);
				}
				else if (Lights[curr_light].attr & LIGHTV1_ATTR_INFINITE)	//无穷远光源
				{
					//需要知道多边形法线以及光源方向

					//顶点按照顺时针排序，所以 u = p0 - p1 v = p0 - p2 n = u x v
					VECTOR4D u, v, n;
					//计算出 u , v
					VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[1], &u);
					VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[2], &v);
					//叉积
					n = VECTOR4D_Cross(&u, &v);

					//首先计算出多边形法线长度
					nl = VECTOR4D_Length_Fast(&n);

					//无穷远处光源
					//公式 L= k * diffuse * (n.l)
					dp = VECTOR4D_Dot(&n, &Lights[curr_light].dir);
					//只有当dp>0即光照射在物体正面时，计算才生效
					if (dp > 0)
					{
						i = 128 * dp / nl;	//防止浮点计算
						r_sum += (Lights[curr_light].c_diffuse.r * r_base * i) / (256 * 128);
						g_sum += (Lights[curr_light].c_diffuse.g * g_base * i) / (256 * 128);
						b_sum += (Lights[curr_light].c_diffuse.b * b_base * i) / (256 * 128);
					}
				}
				else if (Lights[curr_light].attr & LIGHTV1_ATTR_POINT)	//点光源
				{
					//点光源，就是在无穷远光源计算基础上，除以衰减系数 kc + kl * d + kq * d * d

					//需要知道多边形法线以及光源方向

					//顶点按照顺时针排序，所以 u = p0 - p1 v = p0 - p2 n = u x v
					VECTOR4D u, v, n;
					//计算出 u , v
					VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[1], &u);
					VECTOR4D_Build(&curr_poly->tvlist[0], &curr_poly->tvlist[2], &v);
					//叉积
					n = VECTOR4D_Cross(&u, &v);

					//首先计算出多边形法线长度
					nl = VECTOR4D_Length_Fast(&n);

					//计算多边形到光源的向量
					VECTOR4D_Build(&curr_poly->tvlist[0], &Lights[curr_light].pos, &l);
					//根据多边形到光源的向量计算两者间的距离，用来计算衰减因子
					dist = VECTOR4D_Length_Fast(&l);
					//计算光源与多边形表面的夹角
					dp = VECTOR4D_Dot(&n, &l);
					//仅当dp>0时，才进行计算
					if (dp > 0)
					{
						//计算衰减因子 atten = kc + kl * d + kq * d * d
						atten = (Lights[curr_light].kc + Lights[curr_light].kl * dist + Lights[curr_light].kq * dist * dist);
						i = 128 * dp / (nl * dist * atten);
						r_sum += (Lights[curr_light].c_diffuse.r * r_base * i) / (256 * 128);
						g_sum += (Lights[curr_light].c_diffuse.g * g_base * i) / (256 * 128);
						b_sum += (Lights[curr_light].c_diffuse.b * b_base * i) / (256 * 128);
					}
				}
			}
			//判断溢出
			if (r_sum > 255) r_sum = 255;
			if (g_sum > 255) g_sum = 255;
			if (b_sum > 255) b_sum = 255;

			curr_poly->color = { r_sum,g_sum,b_sum };
		}
		else
		{
			curr_poly->color = { r_base,g_base,b_base };
		}
	}
	return (1);
}
