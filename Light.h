#pragma once
#ifndef _LIGHT_
#define _LIGHT_
#include "Primitive3D.h"
//光源常量
#define LIGHTV1_ATTR_AMBIENT	0x0001	//环境光
#define LIGHTV1_ATTR_INFINITE	0x0002	//无穷远光源
#define LIGHTV1_ATTR_POINT		0x0004	//点光源

#define LIGHTV1_STATE_ON		1	//光源开启
#define LIGHTV1_STATE_OFF		0	//光源关闭

#define MAX_LIGHTS				8	//最多光源数

typedef struct LIGHTV1_TYP
{
	int state;	// 光源状态
	int id;		// 光源id
	int attr;	// 光源属性

	RGBAV1 c_ambient;	//环境光强度
	RGBAV1 c_diffuse;	//漫反射光强度
	RGBAV1 c_specular;	//高光反射强度
	POINT4D pos;		//光源位置
	VECTOR4D dir;		//光源方向
	float kc, kl, kq;	//衰减因子
	float spot_inner;	//聚光灯内
	float spot_outer;	//聚光灯外

	float pt;			//聚光灯因子
} LIGHTV1,*LIGHTV1_PTR;
static LIGHTV1 Lights[MAX_LIGHTS];	//光源数组
static int num_lights;	//当前的光源数目
//辅助函数，关闭所有的光源
int Reset_Lights_LIGHTV1(void);
//构建光源
int Init_Light_LIGHTV1(
	int index,	//光源编号
	int _state,	//光源状态
	int _attr,	//光源属性
	RGBAV1	_c_ambient,	//环境光强度
	RGBAV1 _c_diffuse,	//漫反射强度
	RGBAV1 _c_specular,	//高光反射强度
	POINT4D_PTR _pos,	//光源位置
	VECTOR4D_PTR _dir,	//光源方向
	float _kc,	//衰减因子
	float _kl,	//衰减因子
	float _kq,	//衰减因子
	float _spot_inner,	//聚光灯内
	float _spot_outer,	//聚光灯外
	float _pt	//聚光灯因子
);
//对物体进行光照计算,在世界空间下
int Light_OBJECT4DV1_World(OBJECT4DV1_PTR obj,	//物体
	CAM4DV1_PTR cam,	//摄像机
	LIGHTV1_PTR lights,	//光照列表
	int max_lights	//光源总数
);
//对渲染列表进行光照计算，在进行背面剔除后，透视变换前，因为进行透视变换之后，z轴信息损失
int Light_RENDERLIST4DV1_World(RENDERLIST4DV1_PTR rend_list,	//渲染列表
	CAM4DV1_PTR cam,	//相机
	int max_lights	//光源总数
);	
#endif // !_LIGHT_


