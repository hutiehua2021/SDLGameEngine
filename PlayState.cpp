#include "PlayState.h"
#include "Game.h"
#include "PauseGameState.h"
#include "GameOverState.h"
#include "SDLGameObject.h"
#include "StateParser.h"
const std::string PlayState::s_playID = "PLAY";
PlayState::~PlayState()
{
	m_gameObjects.clear();
}
void PlayState::update(float dt)
{
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
	{
		Game::Instance()->getGameStateMachine()->pushState(new PauseGameState());
	}
	for (auto iter : m_gameObjects)
		iter->Update(dt);
}

void PlayState::renderer()
{
	for (auto iter : m_gameObjects)
		iter->Draw();
}

bool PlayState::onEnter()
{
	StateParser stateParser;
	stateParser.ParseState("test.xml", s_playID, m_gameObjects, m_textureIDList);
	std::cout << "������Ϸ״̬\n";
	return true;
}

bool PlayState::onExit()
{
	for (auto iter : m_gameObjects)
		iter->Clean();
	m_gameObjects.clear();
	for (size_t i = 0; i < m_textureIDList.size(); i++)
	{
		TextureManager::Instance()->ClearFromTextureMap(m_textureIDList[i]);
	}
	m_textureIDList.clear();
	return true;
}

const std::string& PlayState::getStateID() const
{
	return s_playID;
}

bool PlayState::checkCollision(SDLGameObject* p1, SDLGameObject* p2)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = p1->getPosition().x;
	rightA = p1->getPosition().x + p1->getWidth();
	topA = p1->getPosition().y;
	bottomA = p1->getPosition().y + p1->getHeight();

	leftB = p2->getPosition().x;
	rightB = p2->getPosition().x + p2->getWidth();
	topB = p2->getPosition().y;
	bottomB = p2->getPosition().y + p2->getHeight();

	if (bottomA <= topB) { return false; }
	if (topA >= bottomB) { return false; }
	if (rightA <= leftB) { return false; }
	if (leftA >= rightB) { return false; }

	return true;
}
