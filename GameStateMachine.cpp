#include "GameStateMachine.h"

GameStateMachine::~GameStateMachine()
{
	m_gameStates.clear();
}

void GameStateMachine::pushState(GameState* pState)
{
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::changeState(GameState* pState)
{
	if (!m_gameStates.empty())
	{
		if (pState->getStateID() == m_gameStates.back()->getStateID())
		{
			return;
		}
		if (m_gameStates.back()->onExit())
		{
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::popState()
{
	if (!m_gameStates.empty())
	{
		if (m_gameStates.back()->onExit())
		{
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
}

void GameStateMachine::update(float dt)
{
	if (!m_gameStates.empty())
	{
		m_gameStates.back()->update(dt);
	}
}

void GameStateMachine::renderer()
{
	if (!m_gameStates.empty())
	{
		m_gameStates.back()->renderer();
	}
}
