#pragma once
#ifndef _CUBE_H_
#define _CUBE_H_
#include "SDLGameObject.h"
#include "InputHandler.h"
#include "GameObjectFactory.h"
#include "Primitive3D.h"
#include "Light.h"
#include "FirstPersonCamera.h"
class CubeFactory : public BaseCreator
{
public:
	virtual GameObject* CreateGameObject() const override;
};
class Cube:public SDLGameObject
{
public:
	Cube() = default;
	Cube(const LoaderParams * pParams) :SDLGameObject(pParams) {
	}
	virtual void Draw();
	virtual void Update(float dt);
	virtual void Clean();
	virtual void InputHandle();

	virtual void Load(const LoaderParams * pParams) override;
private:
	Primitive2D* primitive;
	VECTOR4D vscale = { 1,1,1,1 }, vpos = { 0,0,0,1 }, vrot = { 0,0,0,1 };
	RENDERLIST4DV1 rend_list;
	OBJECT4DV1 obj;
	FirstPersonCamera* camera;
};

#endif // !_CUBE_H_

