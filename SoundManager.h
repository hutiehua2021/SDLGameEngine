#pragma once
#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_
#include <SDL_mixer.h>
#include <map>
#include <iostream>
#include <string>
enum sound_type
{
	SOUND_MUSIC = 0,
	SOUND_SFX = 1,
};
class SoundManager
{
public:
	static SoundManager* Instance()
	{
		if (p_sInstance == 0)
			p_sInstance = new SoundManager();
		return p_sInstance;
	}
	bool Load(const std::string& filenName,const std::string& id,sound_type type);

	void PlaySound(const std::string& id,int Loop);
	void PlayMusic(const std::string& id,int Loop);
private:
	static SoundManager* p_sInstance;
	SoundManager();
	~SoundManager();
private:
	std::map<std::string, Mix_Chunk*> m_sfxs;
	std::map<std::string, Mix_Music*> m_music;
};

#endif // !_SOUNDMANAGER_H_


