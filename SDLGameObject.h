#pragma once
#ifndef _SDLGAMEOBJECT_H_
#define _SDLGAMEOBJECT_H_
#include <SDL.h>
#include "GameObject.h"
#include "TextureManager.h"
#include "EngineMath.h"
class Game;
class SDLGameObject:public GameObject
{
public:
	SDLGameObject() = default;
	SDLGameObject(const LoaderParams* pParams);

	virtual void Draw();
	virtual void Update(float dt);
	virtual void Clean();

	virtual void Load(const LoaderParams* pParams) override;

	int getWidth() const { return m_width; }
	int getHeight() const { return m_height; }
	vec2& getPosition() { return m_postition; }
protected:
	vec2 m_postition;	//位置
	vec2 m_velocity;	//速度
	vec2 m_acceleration;	//加速度

	int m_width;
	int m_height;

	int m_currentFrame;
	int m_currentRow;

	int m_numFrames;
	std::string m_textureId;
	int m_callbackId;
};
#endif	//_SDLGAMEOBJECT_H_

