#pragma once
#ifndef _STATEPARSER_H_
#define _STATEPARSER_H_
#include <iostream>
#include <vector>
#include <string>
#include "tinyxml.h"

class GameObject;

class StateParser
{
public:
	bool ParseState(const char* fileName,const std::string& stateId,std::vector<GameObject*>&pObjects,std::vector<std::string>&pTextureIds);
private:
	void ParseObjects(TiXmlElement* pStateRoot,std::vector<GameObject*>&pObjects);
	void ParseTexture(TiXmlElement* pStateRoot, std::vector<std::string>& pTextureIds);
};
#endif	//_STATEPARSER_H_

