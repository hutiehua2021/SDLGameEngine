#pragma once
#include "GameState.h"
#include <vector>
class GameStateMachine
{
public:
	~GameStateMachine();
	void pushState(GameState* pState);
	void changeState(GameState* pState);
	void popState();

	void update(float dt);
	void renderer();
private:
	std::vector<GameState*> m_gameStates;
};

