#include "MainMenuState.h"


const std::string MainMenuState::s_menuID = "MENU";
void MainMenuState::update(float dt)
{
	for (auto iter : m_gameObjects)
		iter->Update(dt);
}

void MainMenuState::renderer()
{
	for (auto iter : m_gameObjects)
		iter->Draw();
}

bool MainMenuState::onEnter()
{
	StateParser stateParser;
	stateParser.ParseState("test.xml", s_menuID, m_gameObjects, m_textureIDList);
	m_callbacks.push_back(0);
	m_callbacks.push_back(s_menuToPlay);
	m_callbacks.push_back(s_exitFromMenu);
	
	SetCallBack(m_callbacks);

	std::cout << __FILE__ << "\t" << __FUNCDNAME__ << "\n" << "Enter MainMenuState\n";
	return true;
}

bool MainMenuState::onExit()
{
	for (auto iter : m_gameObjects)
		iter->Clean();
	m_gameObjects.clear();
	for (size_t i=0;i<m_textureIDList.size();i++)
	{
		TextureManager::Instance()->ClearFromTextureMap(m_textureIDList[i]);
	}
	m_textureIDList.clear();
	return true;
}

const std::string& MainMenuState::getStateID() const
{
	return s_menuID;
}

void MainMenuState::SetCallBack(const std::vector<Callback>& callbacks)
{
	for (size_t i =0;i<m_gameObjects.size();i++)
	{
		if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
		{
			MenuButton* button = dynamic_cast<MenuButton*>(m_gameObjects[i]);
			button->SetCallBack(callbacks[button->GetCallBackID()]);
		}
	}
}

void MainMenuState::s_menuToPlay()
{
	Game::Instance()->getGameStateMachine()->changeState(new PlayState());
}

void MainMenuState::s_exitFromMenu()
{
	Game::Instance()->Quit();
}
