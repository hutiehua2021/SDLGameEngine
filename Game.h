#pragma once
#ifndef _GAME_H_
#define _GAME_H_
#include <SDL.h>
#include <iostream>
#include <vector>
#include "TextureManager.h"
#include "GameObject.h"
#include "SDLGameObject.h"
#include "Player.h"
#include "InputHandler.h"
#include "GameStateMachine.h"
#include "Primitive3D.h"
#include "FirstPersonCamera.h"
#include "RenderList.h"
class Game
{
private:
	Game() {}
	~Game();
	static Game* s_pInstance;
public:	
	static Game* Instance()
	{
		if (s_pInstance==0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}
		return s_pInstance;
	}
	SDL_Renderer* getRenderer() const { return m_pRenderer; }
	SDL_Surface* getSurface() const { return m_pSurface; }
	SDL_Texture* getTexture() const { return m_pTexture; }
	FirstPersonCamera* getCamera() const { return camera; }
	int GetScreenWidth() const { return m_ScreenWidth; }
	int GetScreenHeight() const { return m_ScreenHeight; }	
	GameStateMachine* getGameStateMachine() const;
public:
	bool Init(const char*, int, int, int, int, bool);
	void Render();
	void Update(float dt);
	void HandleEvent(float dt);
	void Clean();
	void Quit();
	bool Running() const { return m_bRunning; }
private:
	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
	SDL_Surface* m_pSurface;
	SDL_Texture* m_pTexture;
	int m_ScreenWidth;
	int m_ScreenHeight;
	bool m_bRunning;

	GameStateMachine* m_pGameStateMachine;
	Primitive2D* primitive;	//ͼԪ
	FirstPersonCamera* camera;	//���
};
#endif //_GAME_H_

