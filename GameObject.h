#pragma once
#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_
#include <iostream>
#include <string>
class LoaderParams
{
public:
	LoaderParams(int x, int y, int width, int height, std::string textureID,int numFrames = 6,int callbackID = 0,int animSpeed = 0) : m_x(x), m_y(y), m_width(width), m_height(height),m_textureID(textureID), m_numFrames(numFrames),m_callbackID(callbackID),m_animSpeed(animSpeed){
	}

	int getX() const { return m_x; }
	int getY() const { return m_y; }
	int getWidth() const { return m_width; }
	int getHeight() const { return m_height; }
	int getNumFrmes() const { return m_numFrames; }
	int getCallBackId() const { return m_callbackID; }
	int getAnimSpeed() const { return m_animSpeed; }
	std::string getTextureId() const { return m_textureID; }
private:
	int m_x;
	int m_y;

	int m_width;
	int m_height;

	int m_numFrames;

	int m_callbackID;
	int m_animSpeed;
	std::string m_textureID;
};
class GameObject
{
public:
	virtual void Draw() = 0;
	virtual void Update(float dt) = 0;
	virtual void Clean() = 0;

	virtual void Load(const LoaderParams* pParams) = 0;

protected:
	GameObject() = default;
	GameObject(const LoaderParams* pParams) {}
	virtual ~GameObject() {}
};
#endif	//_GAMEOBJECT_H_

