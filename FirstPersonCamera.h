#pragma once
#ifndef  _FIRSTPERSONCAMERA_H_
#define _FIRSTPERSONCAMERA_H_
#include "Primitive3D.h"
//默认值
const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 0.01f;
const float SENSITIVITY = 0.1f;
const float ZOOM = 45.0f;
enum Camera_Movement
{
	CAM_FORWARD,
	CAM_BACKWARD,
	CAM_LEFT,
	CAM_RIGHT
};
class FirstPersonCamera
{
public:
	FirstPersonCamera() = default;
	FirstPersonCamera(float width, float height, VECTOR4D pos = { 0,0,40,1 }, VECTOR4D target = { 0,0,1,1 }, float near = 1.0f, float far = 500.0f, float fov = 45.0f, VECTOR4D up = {0,1,0,1}, float yaw = YAW, float pitch = PITCH):m_MovementSpeed(SPEED), m_MouseSensitivity(SENSITIVITY) {
		Init_CAM4DV1(&camera, CAM_MODEL_UVN, &pos, &target, near, far, fov, width, height);
		m_worldUp = up;
		m_yaw = yaw;
		m_pitch = pitch;

		m_firstMouse = false;
		m_LastX = width / 2.0f;
		m_LastY = height / 2.0f;
		UpdateCameraVectors();
	}
	//获取观察矩阵
	void GetLookAtMatrix(MATRIX4X4_PTR m);
	//获取透视矩阵
	void GetPerPerspectiveMatrix(MATRIX4X4_PTR m);
	//根据枚举值，决定相机移动
	void ProcessKeyBroad(Camera_Movement direction, float deltaTime);
	//获取鼠标坐标，然后更新朝向
	void MoveCameraMouse(float x,float y);
	//获取相机
	CAM4DV1_PTR GetCam() { return &camera; }
private:
	CAM4DV1 camera;
	VECTOR4D m_worldUp;
	VECTOR4D m_right;
	VECTOR4D m_center;
	//欧拉角
	float m_yaw;
	float m_pitch;
	//摄像机选项
	float m_MovementSpeed;
	float m_MouseSensitivity;
	//摄像机最后的位置
	float m_LastX;
	float m_LastY;
	//是否是第一次移动摄像机
	float m_firstMouse;
	//更新朝向
	void UpdateCameraVectors();
	//根据鼠标差值，计算新的朝向，constrainPitch表示锁定仰角
	void ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch = true);
};
#endif // ! _FIRSTPERSONCAMERA_H_

