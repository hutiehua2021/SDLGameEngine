#pragma once
#ifndef _TEXTUEW_MANAGER_H_
#define _TEXTUEW_MANAGER_H_
#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <iostream>
#include <string>

class TextureManager
{
	typedef TextureManager TheTextureManager;
private:
	TextureManager() {}
	~TextureManager();
	static TextureManager* s_pInstance;
public:
	static TextureManager* Instance()
	{
		if (s_pInstance ==0)
		{
			s_pInstance = new TextureManager();
			return s_pInstance;
		}
		return s_pInstance;
	}
	//功能
//加载图片
	bool Load(const std::string& fileName, std::string id, SDL_Renderer* pRenderer);
	//绘制
	void Draw(std::string id, int x, int y, int width, int height, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
	//绘制移动的图片
	void DrawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
	void ClearFromTextureMap(std::string id);
private:
	std::map<std::string, SDL_Texture*> m_textureMap;
};

#endif //_TEXTUEW_MANAGER_H_
