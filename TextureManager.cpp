#include "TextureManager.h"
TextureManager* TextureManager::s_pInstance = 0;

TextureManager::~TextureManager()
{
	m_textureMap.clear();
}

bool TextureManager::Load(const std::string& fileName, std::string id, SDL_Renderer* pRenderer)
{
	SDL_Surface* pTempSurface = IMG_Load(fileName.c_str());
	if (pTempSurface == 0)
	{
		std::cout << __FUNCTION__ << "SDL_Surface 加载失败\n";
		return false;
	}
	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(pRenderer, pTempSurface);
	SDL_FreeSurface(pTempSurface);
	//当加载成功,那么就将texture添加进我们的键值对中
	if (pTexture != 0)
	{
		m_textureMap[id] = pTexture;
		return true;
	}
	return false;
}

void TextureManager::Draw(std::string id, int x, int y, int width, int height, SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
	SDL_Rect srcRect;	//源矩形（文件选取的矩形）
	SDL_Rect destRect;	//目标矩形(渲染到屏幕上的) 

	srcRect.x = 0;
	srcRect.y = 0;
	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;
	destRect.x = x;
	destRect.y = y;

	SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
}

void TextureManager::DrawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
	SDL_Rect srcRect;	//源矩形（文件选取的矩形）
	SDL_Rect destRect;	//目标矩形(渲染到屏幕上的) 

	srcRect.x = width*currentFrame;
	srcRect.y = height*(currentRow - 1);
	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;
	destRect.x = x;
	destRect.y = y;
	
	SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
}

void TextureManager::ClearFromTextureMap(std::string id)
{
	m_textureMap.erase(id);
}
