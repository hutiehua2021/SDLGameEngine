#pragma once
#ifndef _GAMEOBJECTFACTORY_H_
#define _GAMEOBJECTFACTORY_H_
#include <string>
#include <map>
#include "GameObject.h"

class BaseCreator
{
public:
	virtual GameObject* CreateGameObject() const = 0;
	virtual ~BaseCreator(){}
};

class GameObjectFactory
{
private:
	GameObjectFactory() {}
	static GameObjectFactory* s_pInstance;
public:
	static GameObjectFactory* Instance()
	{
		if (s_pInstance == 0)
			s_pInstance = new GameObjectFactory();
		return s_pInstance;
	}

	bool RegisterType(const std::string& typeId,BaseCreator* pCreator);
	GameObject* Create(const std::string& typeId);
	~GameObjectFactory();
private:
	std::map<std::string, BaseCreator*> m_creators;
};
#endif	//_GAMEOBJECTFACTORY_H_


