#pragma once
#ifndef _PLAYSTATE_H_
#define _PLAYSTATE_H_
#include "GameState.h"
#include "GameObject.h"
#include "InputHandler.h"
#include "TextureManager.h"
#include <vector>
class SDLGameObject;
class PlayState :public GameState
{
public:
	PlayState() = default;
	~PlayState();
	virtual void update(float dt);
	virtual void renderer();

	virtual bool onEnter();
	virtual bool onExit();

	virtual const std::string& getStateID() const;

	//���Ժ���
	bool checkCollision(SDLGameObject* p1, SDLGameObject* p2);
private:
	static const std::string s_playID;
private:
	std::vector<GameObject*> m_gameObjects;
};
#endif	//_PLAYSTATE_H_
