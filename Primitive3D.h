#pragma once
#ifndef _PRIMITIVE_3D_H_
#define _PRIMITIVE_3D_H_
#include "EngineMath.h"
#include "Primitive2D.h"
// 多边形排序方式
#define SORT_POLYLIST_AVGZ				0		//按照平均z值进行排序
#define SORT_POLYLIST_NEARZ				1		//按照最小z值进行排序
#define SORT_POLTLIST_FARZ				2		//按照最大z值进行排序
// 定义多边形属性
#define POLY4DV1_ATTR_2SIDED              0x0001	//双面，不需要进行背面删除
#define POLY4DV1_ATTR_TRANSPARENT         0x0002
#define POLY4DV1_ATTR_8BITCOLOR           0x0004
#define POLY4DV1_ATTR_RGB16               0x0008
#define POLY4DV1_ATTR_RGB24               0x0010
//多边形shader模式
#define POLY4DV1_ATTR_SHADE_MODE_PURE       0x0020
#define POLY4DV1_ATTR_SHADE_MODE_CONSTANT   0x0020 // (别名)
#define POLY4DV1_ATTR_SHADE_MODE_FLAT       0x0040
#define POLY4DV1_ATTR_SHADE_MODE_GOURAUD    0x0080
#define POLY4DV1_ATTR_SHADE_MODE_PHONG      0x0100
#define POLY4DV1_ATTR_SHADE_MODE_FASTPHONG  0x0100 // (别名)
#define POLY4DV1_ATTR_SHADE_MODE_TEXTURE    0x0200 


// 多边形和面的属性（可见性）
#define POLY4DV1_STATE_ACTIVE             0x0001
#define POLY4DV1_STATE_CLIPPED            0x0002
#define POLY4DV1_STATE_BACKFACE           0x0004

// 物体数组的数量，主要针对静态数组
#define OBJECT4DV1_MAX_VERTICES           1024  // 64
#define OBJECT4DV1_MAX_POLYS              1024 // 128

// 物体属性（可见性）
#define OBJECT4DV1_STATE_ACTIVE           0x0001
#define OBJECT4DV1_STATE_VISIBLE          0x0002 
#define OBJECT4DV1_STATE_CULLED           0x0004

// 渲染队列中多边形最大数量
#define RENDERLIST4DV1_MAX_POLYS          32768
//变换控制标志
#define TRANSFORM_LOCAL_ONLY       0  //局部/模型顶点变换

#define TRANSFORM_TRANS_ONLY       1  //对变换后的顶点变换

#define TRANSFORM_LOCAL_TO_TRANS   2  //对局部/模型顶点变换，然后将结果保存在变换后的顶点中

//裁剪平面
#define CULL_OBJECT_X_PLANE           0x0001 
#define CULL_OBJECT_Y_PLANE           0x0002 
#define CULL_OBJECT_Z_PLANE           0x0004 
#define CULL_OBJECT_XYZ_PLANES        (CULL_OBJECT_X_PLANE | CULL_OBJECT_Y_PLANE | CULL_OBJECT_Z_PLANE)
//欧拉旋转顺序
#define CAM_ROT_SEQ_XYZ  0
#define CAM_ROT_SEQ_YXZ  1
#define CAM_ROT_SEQ_XZY  2
#define CAM_ROT_SEQ_YZX  3
#define CAM_ROT_SEQ_ZYX  4
#define CAM_ROT_SEQ_ZXY  5
//定义相机投影
#define CAM_PROJ_NORMALIZED        0x0001
#define CAM_PROJ_SCREEN            0x0002
#define CAM_PROJ_FOV90             0x0004

#define CAM_MODEL_EULER            0x0008
#define CAM_MODEL_UVN              0x0010

#define UVN_MODE_SIMPLE            0 
#define UVN_MODE_SPHERICAL         1
////////////////////////////////////////////
//定义识别掩码(CSSD)
#define PLX_RGB_MASK	0x8000	//抽取RGB值	
#define PLX_SHADE_MODE_MASK 0x6000	//抽取着色器	
#define PLX_2SIDED_MASK	0x1000	//抽取单双面状态
#define PLX_COLOR_MASK	0x0fff	//抽取xxxxrrrrggggbbbb的4个分量
//用于颜色模式
#define PLX_COLOR_MODE_RGB_FLAG 0x8000	//多边形使用RGB颜色
#define PLX_COLOR_MODE_INDEXED_FLAG 0x0000	//使用8位颜色
//判断单双面模式
#define PLX_2SIDED_FLAG 0x1000	//多边形是双面的
#define PLX_1SIDED_FLAG	0x0000	//多边形是单面的
//着色模式
#define PLX_SHADE_MODE_PURE_FLAG	0x0000	//固定颜色
#define PLX_SHADE_MODE_FLAT_FLAG	0x2000	//恒定颜色
#define PLX_SHADE_MODE_GOURAUD_FLAG	0x4000	//gourand着色
#define PLX_SHADE_MODE_PHONG_FLAG	0x6000	//phong着色
//////////////////////////////////////////////////////
//颜色
typedef struct RGBAV1_TYP
{
	union
	{
		unsigned int rgba;
		Uint8 rgba_M[4];
		struct {
			Uint8 a, r, g, b;
		};
	};
}RGBAV1,*RGBAV1_PTR;

//多边形数据结构
typedef struct POLY4DV1_TYP
{
	int state;	//设置多边形状态，是否被渲染
	int attr;	//设置多边形物理属性
	VECTOR3D color;	//颜色

	VECTOR4D_PTR vlist;	//顶点列表
	int vert[3];	//顶点元素索引
}POLY4DV1,* POLY4DV1_PTR;
typedef struct POLYF4DV1_TYP
{
	int state;	//状态
	int attr;	//属性
	VECTOR3D color;	//颜色

	VECTOR4D vlist[3];	//三角形顶点
	VECTOR4D tvlist[3];	//变换后的三角形顶点
	POLYF4DV1_TYP* prev;
	POLYF4DV1_TYP* next;
}POLYF4DV1, * POLYF4DV1_PTR;
//渲染对象
typedef struct OBJECT4DV1_TYP
{
	int id;	//识别物体id 
	char name[64];	//物体名称
	int state;	//物体状态
	int attr;	//物体属性
	float avg_radius;	//平均半径
	float max_radius;	//最大半径

	VECTOR4D world_pos;	//世界坐标
	VECTOR4D dir;	//物体方向
	VECTOR4D ux, uy, uz;	//记录朝向

	int num_vertices;	//顶点信息

	VECTOR4D vlist_local[64];	//顶点局部坐标
	VECTOR4D vlist_trans[64];	//变换后的坐标

	int num_polys;	//多边形数目
	POLY4DV1 plist[128];	//多边形数组
}OBJECT4DV1,* OBJECT4DV1_PTR;
//渲染队列
typedef struct RENDERLIST4DV1_TYP
{
	int state;	//渲染列表状态
	int attr;	//渲染列表属性

	//指针数组，指向了一个自包含的多边形面
	POLYF4DV1_PTR poly_ptrs[RENDERLIST4DV1_MAX_POLYS];
	//多边形实际存储位置
	POLYF4DV1 poly_data[RENDERLIST4DV1_MAX_POLYS];
	//实际列表中多边形数目
	int num_polys;
}RENDERLIST4DV1,* RENDERLIST4DV1_PTR;
//相机模型
typedef struct CAM4DV1_TYP
{
	int state;	//相机状态
	int attr;	//相机属性

	VECTOR4D pos;	//相机世界坐标

	VECTOR4D u, v, n;	//uvn相机朝向
	VECTOR4D target;	//uvn相机目标
	VECTOR4D up;		//uvn相机上方
	VECTOR4D front;		//uvn相机前方	

	float view_dist;	//视距

	float fov;

	//远近裁剪面
	float near_clip_z;
	float far_clip_z;

	PLANE3D rt_clip_plane;	//右裁剪平面
	PLANE3D lt_clip_plane;	//左裁剪平面
	PLANE3D tp_clip_plane;	//上裁剪平面
	PLANE3D bt_clip_plane;	//下裁剪平面

	float viewplane_width;	//视平面
	float viewplane_height;

	float viewport_width;	//屏幕宽
	float viewport_height;	//屏幕高
	float viewport_center_x;
	float viewport_center_y;

	//宽高比
	float aspect_ratio;	

	//变换矩阵
	MATRIX4X4 mcam;	//世界到相机变换
	MATRIX4X4 mper;	//相机到投影变换
	MATRIX4X4 mscr;	//投影到屏幕变换
} CAM4DV1, *CAM4DV1_PTR;
//////////////////////////////////////////
//初始化相机
void Init_CAM4DV1(CAM4DV1_PTR cam,int cam_attr,POINT4D_PTR cam_pos, POINT4D_PTR cam_target,float near_clip_z,float far_clip_z,float fov,float viewport_width,float viewport_height);
/////////////////////////////////////////
//3D流水线函数
char* Get_Line_PLG(char* buffer, int maxlength, FILE* fp);

float Compute_OBJECT4DV1_Radius(OBJECT4DV1_PTR obj);

int Load_OBJECT4DV1_PLG(OBJECT4DV1_PTR obj,const char* filename, VECTOR4D_PTR scale,
	VECTOR4D_PTR pos, VECTOR4D_PTR rot);
//对渲染列表进行变换
void Transfrom_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, MATRIX4X4_PTR mt, int coor_select);
//对物体进行变换
void Transform_OBJECT4DV1(OBJECT4DV1_PTR obj, MATRIX4X4_PTR mt, int coord_select, int transform_basis);
//构建局部坐标到世界坐标转换矩阵
void Build_Model_To_World_Mat4(VECTOR4D_PTR vpos, MATRIX4X4_PTR m);
//CVV裁剪 丢弃三角形 这个是在经过透视变换但是没有进行规格化处理的顶点
int Check_CVV(VECTOR4D_PTR v);
//对渲染队列进行CVV裁剪，丢弃三角形，位置在透视变换之后
void Cull_CVV_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list);
//在相机坐标系中对物体进行裁剪 (不在视锥中的物体全部剔除掉)
int Cull_OBJECT4DV1(OBJECT4DV1_PTR obj,CAM4DV1_PTR cam,MATRIX4X4_PTR m,int cull_flags);
//重置剔除状态
void Reset_OBJECT4DV1(OBJECT4DV1_PTR obj);
//重置渲染队列
void Reset_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list);
//将多边形插入渲染队列中
int Insert_POLY4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, POLY4DV1_PTR poly);
//将多半插入
int Insert_POLYF4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, POLYF4DV1_PTR poly);
//将物体插入到渲染列表中 insert_local：是否将局部坐标插入到渲染列表中 lighting_on: 是否打开了光照
int Insert_OBJECT4DV1_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list, OBJECT4DV1_PTR obj,
	int insert_local = 0,int lighting_on = 0);
//对物体进行背面剔除
void Remove_BackFaces_OBJECT4DV1(OBJECT4DV1_PTR obj,CAM4DV1_PTR cam);
//对渲染列表进行背面剔除
void Remove_BackFaces_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list,CAM4DV1_PTR cam);
//构建物体投影矩阵
void Build_Camera_To_Perspective_Matrix4(CAM4DV1_PTR cam, MATRIX4X4_PTR m);
//对物体，将观察空间的顶点变为规格化设备空间再变换到屏幕空间
void View_To_Screen_OBJECT4DV1(OBJECT4DV1_PTR obj,CAM4DV1_PTR cam);
//对渲染列表，将观察空间的顶点变为规格化设备空间再变换到屏幕空间
void View_To_Screen_RENDERLISR4DV1(RENDERLIST4DV1_PTR rend_list,CAM4DV1_PTR cam);
//物体绘制线框
void Draw_OBJECT4DV1_Wrie(OBJECT4DV1_PTR obj,Primitive2D* primitive);
//渲染队列绘制线框
void Draw_RENDERLIST4DV1_Wrie(RENDERLIST4DV1_PTR rend_list,Primitive2D* primitive);
//绘制实心物体
void Draw_OBJECT4DV1_Solid(OBJECT4DV1_PTR obj,Primitive2D* primitive);
//绘制实心渲染队列
void Draw_RENDERLIST4DV1_Solid(RENDERLIST4DV1_PTR rend_list, Primitive2D* primitive);
//根据标识，决定使用哪种深度排序算法
void Sort_RENDERLIST4DV1(RENDERLIST4DV1_PTR rend_list,int sort_method);
//根据平均z值进行排序
int Compare_AvgZ_POLYF4DV1(const void* arg1, const void* arg2);
//根据最大z值进行排序
int Compare_FARZ_POLYF4DV1(const void* arg1, const void* arg2);
//根据最小z值进行排序
int Compare_NEARZ_POLYF4DV1(const void* arg1, const void* arg2);
/////////////////////////////////////////
#endif
