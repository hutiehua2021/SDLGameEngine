#include "Cube.h"
#include "Game.h"
#include "RenderList.h"
static MATRIX4X4 mrot = IMAT_4X4;
static float alpha = 1.0f;
void Cube::Draw()
{
	Reset_OBJECT4DV1(&obj);
	//世界坐标变换
	MATRIX_Set_Rotate(&mrot, -1, -0.5, 1, alpha);
	Transform_OBJECT4DV1(&obj, &mrot, TRANSFORM_LOCAL_TO_TRANS, 1);
	Build_Model_To_World_Mat4(&obj.world_pos, &mrot);
	Transform_OBJECT4DV1(&obj, &mrot, TRANSFORM_TRANS_ONLY, 1);
	//插入到渲染队列中
	RenderList::Instance()->InsertRenderList(&obj,Game::Instance()->getCamera());
}
void Cube::Update(float dt)
{
	alpha += 0.01f;
}

void Cube::Clean()
{
}

void Cube::InputHandle()
{
}

void Cube::Load(const LoaderParams* pParams)
{
	SDLGameObject::Load(pParams);
	//初始化相机
	camera = new FirstPersonCamera(Game::Instance()->GetScreenWidth(), Game::Instance()->GetScreenHeight());
	//加载物体

	Load_OBJECT4DV1_PLG(&obj, "assets/cube2.plg", &vscale, &vpos, &vrot);
	//生成光源
	int ambient_light = Init_Light_LIGHTV1(0, LIGHTV1_STATE_ON, LIGHTV1_ATTR_AMBIENT, { 0x00000000 }, { 0 }, { 0 }, NULL, NULL, 0, 0, 0, 0, 0, 0);
	VECTOR4D sun_dir = { 0,0,10,0 };
	int sun_light = Init_Light_LIGHTV1(1, LIGHTV1_STATE_ON, LIGHTV1_ATTR_INFINITE, { 0 }, { 0xffffffff }, { 0 }, NULL, &sun_dir, 0, 0, 0, 0, 0, 0);
	 sun_dir = { 0,10,10,0 };
	sun_light = Init_Light_LIGHTV1(2, LIGHTV1_STATE_ON, LIGHTV1_ATTR_INFINITE, { 0 }, { 0xffffffff }, { 0 }, NULL, &sun_dir, 0, 0, 0, 0, 0, 0);

	obj.world_pos = { m_postition.x,m_postition.y,0,1 };
}

GameObject* CubeFactory::CreateGameObject() const
{
	return new Cube();
}
