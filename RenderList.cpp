#include "RenderList.h"

RenderList* RenderList::p_sInstance = 0;
static MATRIX4X4 mrot = IMAT_4X4;
void RenderList::DrawRenderList(FirstPersonCamera* camera,Primitive2D* primitive)
{
	Remove_BackFaces_RENDERLIST4DV1(&rend_list, camera->GetCam());
	Light_RENDERLIST4DV1_World(&rend_list, camera->GetCam(), 3);
	//世界到相机变换
	camera->GetLookAtMatrix(&mrot);
	Transfrom_RENDERLIST4DV1(&rend_list, &mrot, TRANSFORM_TRANS_ONLY);
	//在相机空间下，对多边形进行深度排序
	Sort_RENDERLIST4DV1(&rend_list,SORT_POLYLIST_AVGZ);
	//相机到投影变换
	camera->GetPerPerspectiveMatrix(&mrot);	//构建投影矩阵
	Transfrom_RENDERLIST4DV1(&rend_list, &mrot, TRANSFORM_TRANS_ONLY);
	//投影到屏幕变换
	View_To_Screen_RENDERLISR4DV1(&rend_list, camera->GetCam());
	//绘制渲染队列
	Draw_RENDERLIST4DV1_Solid(&rend_list, primitive);
}

void RenderList::ResetRenderList()
{
	Reset_RENDERLIST4DV1(&rend_list);
}

void RenderList::InsertRenderList(OBJECT4DV1_PTR obj,FirstPersonCamera* camera)
{
	camera->GetLookAtMatrix(&mrot);
	if (!Cull_OBJECT4DV1(obj, camera->GetCam(),&mrot, CULL_OBJECT_XYZ_PLANES))
		Insert_OBJECT4DV1_RENDERLIST4DV1(&rend_list, obj, 0, 1);
}