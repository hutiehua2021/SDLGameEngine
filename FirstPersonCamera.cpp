#include "FirstPersonCamera.h"

void FirstPersonCamera::GetLookAtMatrix(MATRIX4X4_PTR m)
{
	VECTOR4D_Add(&camera.pos, &camera.front, &m_center);
	LookAt(m, &camera.pos, &m_center, &camera.up);
}

void FirstPersonCamera::GetPerPerspectiveMatrix(MATRIX4X4_PTR m)
{
	Build_Camera_To_Perspective_Matrix4(&camera, m);
}

void FirstPersonCamera::ProcessKeyBroad(Camera_Movement direction, float deltaTime)
{
	float velocity = m_MovementSpeed * deltaTime;
	VECTOR4D front, right;
	VECTOR4D_COPY(&front, &camera.front);
	VECTOR4D_COPY(&right, &m_right);
	if (direction == CAM_FORWARD)
	{
		VECTOR4D_Scale(velocity, &front);
		camera.pos = VECTOR4D_Add(&camera.pos, &front);
	}
	if (direction == CAM_BACKWARD)
	{
		VECTOR4D_Scale(velocity, &front);
		camera.pos = VECTOR4D_Sub(&camera.pos, &front);
	}
	if (direction == CAM_LEFT)
	{
		VECTOR4D_Scale(velocity, &right);
		camera.pos = VECTOR4D_Sub(&camera.pos, &right);
	}
	if (direction == CAM_RIGHT)
	{
		VECTOR4D_Scale(velocity, &right);
		camera.pos = VECTOR4D_Add(&camera.pos, &right);
	}
}

void FirstPersonCamera::MoveCameraMouse(float x, float y)
{
	if (m_firstMouse)
	{
		m_LastX = x;
		m_LastY = y;
		m_firstMouse = false;
	}
	float xoffset = x - m_LastX;
	float yoffset = m_LastY - y;
	m_LastX = x;
	m_LastY = y;

	ProcessMouseMovement(xoffset, yoffset);
}

void FirstPersonCamera::UpdateCameraVectors()
{
	VECTOR4D front = { 0,0,0,1 };
	front.x = cos(DEG_TO_RAD(m_yaw)) * cos(DEG_TO_RAD(m_pitch));
	front.y = sin(DEG_TO_RAD(m_pitch));
	front.z = cos(DEG_TO_RAD(m_pitch)) * sin(DEG_TO_RAD(m_yaw));
	VECTOR4D_Normalize(&front);
	VECTOR4D_COPY(&camera.front, &front);

	//计算Right向量
	VECTOR4D_Cross(&camera.front, &m_worldUp, &m_right);
	VECTOR4D_Normalize(&m_right);
	//计算up向量
	VECTOR4D_Cross(&m_right, &camera.front, &camera.up);
	VECTOR4D_Normalize(&camera.up);
}

void FirstPersonCamera::ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch)
{
	xoffset *= m_MouseSensitivity;
	yoffset *= m_MouseSensitivity;
	m_yaw += xoffset;
	m_pitch += yoffset;
	if (constrainPitch)
	{
		if (m_pitch > 89.0f)
			m_pitch = 89.0f;
		if (m_pitch < -89.0f)
			m_pitch = -89.0f;
	}
	//更新朝向
	UpdateCameraVectors();
}
