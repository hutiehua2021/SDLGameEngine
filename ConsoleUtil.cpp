#include "ConsoleUtil.h"


FILE* fperror;
int Write_Error(const char* string, ...)
{
	// this function prints out the error string to the error file
	fperror = fopen("debug.txt", "a");

	char buffer[1024]; // working buffer

	va_list arglist; // variable argument list

	// make sure both the error file and string are valid
	if (!string || !fperror)
		return(0);

	// print out the string using the variable number of arguments on stack
	va_start(arglist, string);
	vsprintf(buffer, string, arglist);
	va_end(arglist);

	// write string to file
	fprintf(fperror, buffer);

	// flush buffer incase the system bails
	fflush(fperror);

	// return success
	return(1);;
}
