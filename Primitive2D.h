#pragma once
#ifndef _PRIMITIVE2D_H_
#define _PRIMITIVE2D_H_
#include <algorithm>
#include <cmath>
#include <SDL.h>
#include <vector>
#include <deque>
#include <iostream>
#include <thread>
#include "EngineMath.h"
#include "DoubleLink.hpp"

#define CORE_NUM 4
class POLYGON2D_TPY
{
public:
	int state;	//标志多边形状态
	int num_vertx;	//多边形顶点数
	float x0, y0;	//多边形中心
	float x, y;	//坐标
	int xoffset = 0,yoffset = 0;	//偏移
	float xv, yv;	//方向
	float mass;	//质量
	vec3 color;	//颜色
	vec2* vlist;	//顶点列表
};
using POLYGON2D = POLYGON2D_TPY;
//用来记录顶点状态
class VertexState
{
public:
	vec2 vec;	//顶点
	bool isConvex = false;	//是否是凸点
	bool isSeparable = false;	//是否是
	VertexState() = default;
	VertexState(vec2& v):vec(v){}
};
class Primitive2D
{
public:
	Primitive2D(SDL_Renderer* render,SDL_Surface* surface,SDL_Texture* texture) :pRender(render),pSurface(surface),pTexture(texture) {}
	//DDA算法
	void DDALine(const vec2& start,const vec2& end,const VECTOR3D& color);
	//Breaseham算法
	void BreaseLine(vec2&& start,vec2&& end,const VECTOR3D& color);

	void DrawLine(int x1,int y1,int x2,int y2,const VECTOR3D& color);
	//裁剪画线
	void ClipDrawLine(vec2&& start,vec2&& end,const VECTOR3D& color);
	//设置裁剪区域
	void SetClipRect(const vec2& min, const vec2& max) { mClipMin = min, mClipMax = max; }
	//绘制三角形
	//平顶三角形填充
	void DrawTopTri(int x1, int y1, int x2, int y2, int x3, int y3, const VECTOR3D& color);
	//平底三角形填充
	void DrawBottomTri(int x1, int y1, int x2, int y2, int x3, int y3, const VECTOR3D& color);
	//任意三角形填充
	void DrawTriangle2D(const VECTOR2D& p1, const VECTOR2D& p2, const VECTOR2D& p3, const VECTOR3D& color);
private:
	SDL_Renderer* pRender;
	SDL_Surface* pSurface;
	SDL_Texture* pTexture;
	vec2 mClipMin;	//最小裁剪区域
	vec2 mClipMax;	//最大裁剪区域
	inline void SetPixel(int x, int y) {
		SDL_RenderDrawPoint(pRender, x, y);
	}
	//对线段进行裁剪
	int ClipLine(vec2& p1,vec2& p2);
	void DrawLine_Thread(int p,int x1,int y1,int x2,int y2,int dx,int dy);
};
#endif	//_PRIMITIVE2D_H_
