#pragma once
#ifndef _MAINMENUSTATE_H_
#define _MAINMENUSTATE_H_
#include "MenuState.h"
#include "StateParser.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "Game.h"
class MainMenuState :public MenuState
{
public:
	virtual void update(float dt) override;
	virtual void renderer() override;

	virtual bool onEnter() override;
	virtual bool onExit() override;

	virtual const std::string& getStateID() const override;

private:
	virtual void SetCallBack(const std::vector<Callback>& callbacks) override;

	//�ص�����
	static void s_menuToPlay();
	static void s_exitFromMenu();

	static const std::string s_menuID;

	std::vector<GameObject*> m_gameObjects;
};
#endif	//_MAINMENUSTATE_H_


