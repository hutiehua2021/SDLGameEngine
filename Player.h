#pragma once
#ifndef _PLAYER_H_
#define _PLAYER_H_
#include "SDLGameObject.h"
#include "InputHandler.h"
#include "GameObjectFactory.h"
#include "Primitive3D.h"
// defines for the game universe
#define UNIVERSE_RADIUS   4000

#define POINT_SIZE        200
#define NUM_POINTS_X      (2*UNIVERSE_RADIUS/POINT_SIZE)
#define NUM_POINTS_Z      (2*UNIVERSE_RADIUS/POINT_SIZE)
#define NUM_POINTS        (NUM_POINTS_X*NUM_POINTS_Z)

// defines for objects
#define NUM_TOWERS        24
#define NUM_TANKS         24
#define TANK_SPEED        15

class PlayerFactory :public BaseCreator
{
public:
	virtual GameObject* CreateGameObject() const override;
};
class Player:public SDLGameObject
{
public:
	Player() = default;
	Player(const LoaderParams* pParams) :SDLGameObject(pParams) {
	}
	
	virtual void Draw();
	virtual void Update(float dt);
	virtual void Clean();
	virtual void InputHandle();

	virtual void Load(const LoaderParams* pParams) override;
private:
	Primitive2D* primitive;
};
#endif	//_PLAYER_H_


