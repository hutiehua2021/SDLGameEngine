#include "Primitive2D.h"

void Primitive2D::DDALine(const vec2& start, const vec2& end,const VECTOR3D& color)
{
	SDL_SetRenderDrawColor(pRender, (Uint8)color.x, (Uint8)color.y, (Uint8)color.z, 0xff);
	int dx = end.x - start.x;
	int dy = end.y - start.y;
	int steps; 
	float xIncrement, yIncrement; //增量
	float x = start.x, y = start.y;
	if (fabs(dx) > fabs(dy))	//以x为步长
		steps = fabs(dx);
	else
		steps = fabs(dy);	//以y为步长
	xIncrement = float(dx) / float(steps);
	yIncrement = float(dy) / float(steps);
	SetPixel(x, y);
	for (int k=0;k<steps;++k)
	{
		x += xIncrement;
		y += yIncrement;
		SetPixel(x, y);
	}
	return;
}
void Primitive2D::DrawLine_Thread(int p, int x1, int y1, int x2, int y2, int dx, int dy)
{
	int twoDy = 2 * dy, twoMinuDx = 2 * (dy - dx), twoDx = 2 * dx, twoMinuDy = 2 * (dx - dy);
	int twoSum = 2 * (dy + dx);
	int x = x1, y = y1;
	SetPixel(x, y);
	while (x<x2)
	{
		++x;
		if (p < 0)
			p += twoDy;
		else
		{
			++y;
			p += twoMinuDx;
		}
		SetPixel(x, y);
	}
	return;
}
void Primitive2D::BreaseLine(vec2&& start, vec2&& end,const VECTOR3D& color)
{
	SDL_SetRenderDrawColor(pRender, (Uint8)color.x, (Uint8)color.y, (Uint8)color.z, 0xff);
	int x, y, rem = 0;
	if (start.x == end.x && start.y == end.y) {
		SetPixel(start.x, start.y);
	}
	else if (start.x == end.x) {
		int inc = (start.y <= end.y) ? 1 : -1;
		for (y = start.y; y != end.y; y += inc) SetPixel( start.x, y);
		SetPixel(end.x,end.y);
	}
	else if (start.y == end.y) {
		int inc = (start.x <= end.x) ? 1 : -1;
		for (x = start.x; x != end.x; x += inc) SetPixel(x, start.y);
		SetPixel(end.x, end.y);
	}
	else {
		int dx = (start.x < end.x) ? end.x - start.x : start.x - end.x;
		int dy = (start.y < end.y) ? end.y - start.y : start.y - end.y;
		if (dx >= dy) {
			if (end.x < start.x) x = start.x, y = start.y, start.x = end.x, start.y = end.y, end.x = x, end.y = y;
			for (x = start.x, y = start.y; x <= end.x; x++) {
				SetPixel(x, y);
				rem += dy;
				if (rem >= dx) {
					rem -= dx;
					y += (end.y >= start.y) ? 1 : -1;
					SetPixel(x, y);
				}
			}
			SetPixel(end.x, end.y);
		}
		else {
			if (end.y < start.y) x = start.x, y = start.y, start.x = end.x, start.y = end.y, end.x = x, end.y = y;
			for (x = start.x, y = start.y; y <= end.y; y++) {
				SetPixel(x, y);
				rem += dx;
				if (rem >= dy) {
					rem -= dy;
					x += (end.x >= start.x) ? 1 : -1;
					SetPixel(x, y);
				}
			}
			SetPixel(end.x, end.y);
		}
	}
}

void Primitive2D::DrawLine(int x1, int y1, int x2, int y2, const VECTOR3D& color)
{
	SDL_SetRenderDrawColor(pRender, (Uint8)color.x, (Uint8)color.y, (Uint8)color.z, 0xff);
	int x, y, rem = 0;
	if (x1 == x2 && y1 == y2) {
		SetPixel(x1, y1);
	}
	else if (x1 == x2) {
		int inc = (y1 <= y2) ? 1 : -1;
		for (y = y1; y != y2; y += inc) SetPixel(x1, y);
		SetPixel(x2, y2);
	}
	else if (y1 == y2) {
		int inc = (x1 <= x2) ? 1 : -1;
		for (x = x1; x != x2; x += inc) SetPixel(x, y1);
		SetPixel(x2, y2);
	}
	else {
		int dx = (x1 < x2) ? x2 - x1 : x1 - x2;
		int dy = (y1 < y2) ? y2 - y1 : y1 - y2;
		if (dx >= dy) {
			if (x2 < x1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
			for (x = x1, y = y1; x <= x2; x++) {
				SetPixel( x, y);
				rem += dy;
				if (rem >= dx) {
					rem -= dx;
					y += (y2 >= y1) ? 1 : -1;
					SetPixel(x, y);
				}
			}
			SetPixel(x2, y2);
		}
		else {
			if (y2 < y1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
			for (x = x1, y = y1; y <= y2; y++) {
				SetPixel(x, y);
				rem += dx;
				if (rem >= dy) {
					rem -= dy;
					x += (x2 >= x1) ? 1 : -1;
					SetPixel(x, y);
				}
			}
			SetPixel( x2, y2);
		}
	}
}

void Primitive2D::ClipDrawLine(vec2&& start, vec2&& end,const VECTOR3D& color)
{
	if (!ClipLine(start, end))
		return;
	DrawLine(start.x,start.y,end.x,end.y, color);
}

void Primitive2D::DrawTopTri(int x1, int y1, int x2, int y2, int x3, int y3, const VECTOR3D& color)
{
	float dx_right,    
		dx_left,    
		xs, xe,      
		height;     

	int temp_x,        
		temp_y,
		right,        
		left;

	if (x2 < x1)
	{
		temp_x = x2;
		x2 = x1;
		x1 = temp_x;
	}

	height = y3 - y1;

	dx_left = (x3 - x1) / height;
	dx_right = (x3 - x2) / height;

	xs = (float)x1;
	xe = (float)x2 + (float)0.5;

	if (y1 < mClipMin.y)
	{
		xs = xs + dx_left * (float)(-y1 + mClipMin.y);
		xe = xe + dx_right * (float)(-y1 + mClipMin.y);

		y1 = mClipMin.y;

	}

	if (y3 > mClipMax.y)
		y3 = mClipMax.y;

	if (x1 >= mClipMin.x && x1 <= mClipMax.x &&
		x2 >= mClipMin.x && x2 <= mClipMax.x &&
		x3 >= mClipMin.x && x3 <= mClipMax.x)
	{
		for (temp_y = y1; temp_y <= y3; temp_y++)
		{
			ClipDrawLine(vec2((int)(xs + 0.5), temp_y), vec2((int)(xe + 0.5), temp_y), color);
			xs += dx_left;
			xe += dx_right;

		}
	}
	else
	{
		for (temp_y = y1; temp_y <= y3; temp_y++)
		{
			left = (int)xs;
			right = (int)xe;

			xs += dx_left;
			xe += dx_right;

			if (left < mClipMin.x)
			{
				left = mClipMin.x;

				if (right < mClipMin.x)
					continue;
			}

			if (right > mClipMax.x)
			{
				right = mClipMax.x;

				if (left > mClipMax.x)
					continue;
			}
			ClipDrawLine(vec2((int)(left + 0.5), temp_y), vec2((int)(right + 0.5), temp_y), color);
		} 

	}
}

void Primitive2D::DrawBottomTri(int x1, int y1, int x2, int y2, int x3, int y3, const VECTOR3D& color)
{
	float dx_right;	//right edge x step
	float dx_left;	//left edge x step
	float xe, xs;
	float height;

	int right, left;
	//这里需要关注一下三角形结构(平面在下)
	if (x3 < x2)
		std::swap(x3, x2);
	//接下来计算1/m
	height = y3 - y1;
	//由于p1,p2位于同一水平线上
	dx_left = (x2 - x1) / height;
	dx_right = (x3 - x1) / height;
	//接下来计算开始位置
	xs = (float)x1;
	xe = (float)x1;
	//对y轴进行裁剪
	if (y1 < mClipMin.y)
	{
		//计算出新的开始位置
		xs = xs + dx_left * (float)(-y1 + mClipMin.y);
		xe = xe + dx_right * (float)(-y1 + mClipMin.y);
		//std::cout << "新的x坐标:" << xs << '\t' << xe << std::endl;
		//重设y位置
		y1 = mClipMin.y;
	}
	if (y3 > mClipMax.y)
		y3 = mClipMax.y;
	if (x1 >= mClipMin.x && x1 <= mClipMax.x &&
		x2 >= mClipMin.x && x2 <= mClipMax.x &&
		x3 >= mClipMin.x && x3 <= mClipMax.x)
	{
		//扫描
		for (int y = y1; y <= y3; ++y)
		{
			ClipDrawLine(vec2((int)(xs + 0.5), y), vec2((int)(xe + 0.5), y), color);
			xs += dx_left;
			xe += dx_right;
		}
	}
	else
	{
		//x轴需要被裁剪
		for (int y = y1; y <= y3; ++y)
		{
			left = (int)xs;
			right = (int)xe;

			xs += dx_left;
			xe += dx_right;
			//对直线进行裁剪
			if (left < mClipMin.x)
			{
				left = mClipMin.x;
				if (right < mClipMin.x)
				{
					continue;	//丢弃这条线
				}
			}
			if (right > mClipMax.x)
			{
				right = mClipMax.x;
				if (left > mClipMax.x)
				{
					continue;;
				}
			}
			ClipDrawLine(vec2((int)(left + 0.5), y), vec2((int)(right + 0.5), y), color);
		}
	}
}

void Primitive2D::DrawTriangle2D(const VECTOR2D& p1, const VECTOR2D& p2, const VECTOR2D& p3 , const VECTOR3D& color)
{
	int new_x = 0;		//分割点
	if ((p1.x == p2.x && p2.x == p3.x) || (p1.y == p2.y && p2.y == p3.y))
		return;
	VECTOR2D pt1 = p1;
	VECTOR2D pt2 = p2;
	VECTOR2D pt3 = p3;
	//对p1,p2,p3顶点进行排序（这里是dx坐标系）
	//p3>p2>p1
	if (pt2.y < pt1.y)
		std::swap(pt1, pt2);
	if (pt3.y < pt1.y)
		std::swap(pt3, pt1);
	if (pt3.y < pt2.y)
		std::swap(pt3, pt2);
	//全在边界外则剔除
	if (pt3.y<mClipMin.y || pt1.y>mClipMax.y ||
		(pt1.x < mClipMin.x && pt2.x < mClipMin.x && pt3.x < mClipMin.x) ||
		(pt1.x > mClipMax.x&& pt2.x > mClipMax.x&& pt3.x > mClipMax.x))
		return;
	//对几种情况进行分类
	if (pt1.y == pt2.y)	//是平顶三角形
	{
		DrawTopTri(pt1.x, pt1.y, pt2.x, pt2.y, pt3.x, pt3.y, color);
	}
	else if (pt2.y == pt3.y)	//是平底三角形
	{
		DrawBottomTri(pt1.x, pt1.y, pt2.x, pt2.y, pt3.x, pt3.y, color);
	}
	else
	{
		//x=1/m*(y - y0)+x0;
		new_x = pt1.x + (int)(0.5 + (float)(pt2.y - pt1.y) * (float)(pt3.x - pt1.x) / (float)(pt3.y - pt1.y));;
		DrawBottomTri(pt1.x, pt1.y, new_x, pt2.y, pt2.x, pt2.y, color);
		DrawTopTri(pt2.x, pt2.y, new_x, pt2.y, pt3.x, pt3.y, color);
	}
}

int Primitive2D::ClipLine(vec2& p1, vec2& p2)
{
	//定义9个区域
#define CLIP_CODE_C 0x0000
#define CLIP_CODE_N 0x0008
#define CLIP_CODE_S 0x0004
#define CLIP_CODE_E 0x0002
#define CLIP_CODE_W 0x0001

#define CLIP_CODE_NE 0x000a
#define CLIP_CODE_SE 0x0006
#define CLIP_CODE_NW 0x0009
#define CLIP_CODE_SW 0x0005

	vec2 pc1(p1.x,p1.y);
	vec2 pc2(p2.x,p2.y);

	int p1_code = 0;
	int p2_code = 0;

	//判断线段是否越界(以dx坐标)
	if (p1.y < mClipMin.y)
		p1_code |= CLIP_CODE_N;
	else if (p1.y > mClipMax.y)
		p1_code |= CLIP_CODE_S;
	else if (p1.x < mClipMin.x)
		p1_code |= CLIP_CODE_W;
	else if (p1.x > mClipMax.x)
		p1_code |= CLIP_CODE_E;

	if (p2.y < mClipMin.y)
		p2_code |= CLIP_CODE_N;
	else if (p2.y > mClipMax.y)
		p2_code |= CLIP_CODE_S;
	else if (p2.x < mClipMin.x)
		p2_code |= CLIP_CODE_W;
	else if (p2.x > mClipMax.x)
		p2_code |= CLIP_CODE_E;
	//如果都在区域内，就不需要裁剪
	if (p1_code == 0 && p2_code == 0)
		return 1;
	//全不可见
	if (p1_code & p2_code)
		return 0;
	//第一个点
	switch (p1_code)
	{
	case CLIP_CODE_C:
		break;
	case  CLIP_CODE_N:	
		pc1.y = mClipMin.y;
		pc1.x = p1.x + 0.5 + (mClipMin.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		break;
	case CLIP_CODE_S:
		pc1.y = mClipMax.y;
		pc1.x = p1.x + 0.5 + (mClipMax.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		break;
	case CLIP_CODE_W:
		pc1.x = mClipMin.x;
		pc1.y = p1.y + 0.5 + (mClipMin.x - p1.x) * (p2.y - p1.y)/ (p2.x - p1.x);
		break;
	case CLIP_CODE_E:
		pc1.x = mClipMax.x;
		pc1.y = p1.y + 0.5 + (mClipMax.x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
		break;
		//接下来的情况可能需要裁剪2次
	case CLIP_CODE_NE:
		pc1.y = mClipMin.y;
		pc1.x = p1.x + 0.5 + (mClipMin.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc1.x<mClipMin.x||pc1.x>mClipMax.x)
		{
			pc1.x = mClipMax.x;
			pc1.y = p1.y + 0.5 + (mClipMax.x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_SE:
		pc1.y = mClipMax.y;
		pc1.x = p1.x + 0.5 + (mClipMax.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc1.x<mClipMin.x || pc1.x>mClipMax.x)
		{
			pc1.x = mClipMax.x;
			pc1.y = p1.y + 0.5 + (mClipMax.x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_NW:
		pc1.y = mClipMin.y;
		pc1.x = p1.x + 0.5 + (mClipMin.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc1.x<mClipMin.x||pc1.x>mClipMax.x)
		{
			pc1.x = mClipMin.x;
			pc1.y = p1.y + 0.5 + (mClipMin.x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_SW:
		pc1.y = mClipMax.y;
		pc1.x = p1.x + 0.5 + (mClipMax.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc1.x<mClipMin.x || pc1.x>mClipMax.x)
		{
			pc1.x = mClipMin.x;
			pc1.y = p1.y + 0.5 + (mClipMin.x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	default:
		break;
	}
	//第二个点
	switch (p2_code)
	{
	case CLIP_CODE_C:
		break;
	case  CLIP_CODE_N:
		pc2.y = mClipMin.y;
		pc2.x = p2.x + 0.5 + (mClipMin.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		break;
	case CLIP_CODE_S:
		pc2.y = mClipMax.y;
		pc2.x = p2.x + 0.5 + (mClipMax.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		break;
	case CLIP_CODE_W:
		pc2.x = mClipMin.x;
		pc2.y = p2.y + 0.5 + (mClipMin.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		break;
	case CLIP_CODE_E:
		pc2.x = mClipMax.x;
		pc2.y = p2.y + 0.5 + (mClipMax.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		break;
		//接下来的情况可能需要裁剪2次
	case CLIP_CODE_NE:
		pc2.y = mClipMin.y;
		pc2.x = p2.x + 0.5 + (mClipMin.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc2.x<mClipMin.x || pc2.x>mClipMax.x)
		{
			pc2.x = mClipMax.x;
			pc2.y = p2.y + 0.5 + (mClipMax.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_SE:
		pc2.y = mClipMax.y;
		pc2.x = p2.x + 0.5 + (mClipMax.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc2.x<mClipMin.x || pc2.x>mClipMax.x)
		{
			pc2.x = mClipMax.x;
			pc2.y = p2.y + 0.5 + (mClipMax.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_NW:
		pc2.y = mClipMin.y;
		pc2.x = p2.x + 0.5 + (mClipMin.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc2.x<mClipMin.x || pc2.x>mClipMax.x)
		{
			pc2.x = mClipMin.x;
			pc2.y = p2.y + 0.5 + (mClipMin.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	case CLIP_CODE_SW:
		pc2.y = mClipMax.y;
		pc2.x = p2.x + 0.5 + (mClipMax.y - p2.y) * (p2.x - p1.x) / (p2.y - p1.y);
		if (pc2.x<mClipMin.x || pc2.x>mClipMax.x)
		{
			pc2.x = mClipMin.x;
			pc2.y = p2.y + 0.5 + (mClipMin.x - p2.x) * (p2.y - p1.y) / (p2.x - p1.x);
		}
		break;
	default:
		break;
	}
	//再次判断新的点是否被剔除
	if ((pc1.x<mClipMin.x)||(pc1.x>mClipMax.x)||(pc1.y<mClipMin.y)||(pc1.y>mClipMax.y)||(pc2.x<mClipMin.x)||(pc2.x>mClipMax.x)||(pc2.y<mClipMin.y)||(pc2.y>mClipMax.y))
		return 0;
	//保存结果
	p1.x = pc1.x;
	p1.y = pc1.y;
	p2.x = pc2.x;
	p2.y = pc2.y;

	return 1;
}